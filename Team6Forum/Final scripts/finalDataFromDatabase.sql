-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.5.18-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping data for table team_forum_6.comments: ~46 rows (approximately)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` (`comment_id`, `user_id`, `post_id`, `content`, `creation_date`) VALUES
	(9, 6, 3, 'SQL allows the use of wildcard characters', '2023-02-26 10:10:31'),
	(15, 3, 7, 'I would be interested to know how you would make this responsive if you had a few more columns or where the data is a little longer.', '2023-03-20 00:00:00'),
	(22, 3, 3, 'Plain Old Java Object The name is used to emphasize that a given object is an ordinary Java Object', '2023-03-22 00:00:00'),
	(23, 3, 3, 'Java ArrayList class can contain duplicate elements.', '2023-03-22 00:00:00'),
	(25, 3, 3, 'new Plain Old Java Object The name is used to emphasize that a given object is an ordinary Java Object', '2023-03-22 00:00:00'),
	(26, 3, 3, 'change Plain Old Java Object The name is used to emphasize that a given object is an ordinary Java Object', '2023-03-22 00:00:00'),
	(27, 3, 3, 'change Plain Old Java Object The name is used to emphasize that a given object is an ordinary Java Object', '2023-03-22 00:00:00'),
	(28, 17, 3, 'lalalallaa Plain Old Java Object The name is used to emphasize that a given object is an ordinary Java Object', '2023-03-22 00:00:00'),
	(36, 17, 7, 'It is used to remove the element present at the specified position in the list.', '2023-03-29 14:32:12'),
	(37, 17, 7, 'In ArrayList, manipulation is a little bit slower than the LinkedList in Java because a lot of shifting needs to occur if any element is removed from the array list.', '2023-03-25 00:00:00'),
	(38, 17, 7, 'Yes Java ArrayList allows random access because the array works on an index basis.', '2023-03-25 00:00:00'),
	(39, 17, 7, 'I would be interested to know how you would make this responsive if you had a few more columns or where the data is a little longer.', '2023-03-25 00:00:00'),
	(40, 17, 9, 'It is used to enhance the capacity of an ArrayList instance.', '2023-03-29 14:32:30'),
	(111, 7, 4, 'It is used to replace the specified element in the list.', '2023-03-26 11:06:20'),
	(112, 7, 10, 'It is used to sort the elements of the list on the basis of the specified comparator.', '2023-03-26 11:06:20'),
	(113, 7, 19, 'It is used to create a spliterator over the elements in a list.', '2023-03-26 11:06:20'),
	(114, 7, 12, 'It is used to fetch all the elements that lies within the given range.\n', '2023-03-26 11:06:20'),
	(115, 8, 4, 'It is used to return the number of elements present in the list.\n', '2023-03-26 11:06:20'),
	(116, 8, 10, 'It is used to trim the capacity of this ArrayList instance to be the list\'s current size.\n', '2023-03-26 11:06:20'),
	(117, 8, 19, 'This method returns the element at the specified position in this list.', '2023-03-26 11:06:20'),
	(118, 9, 4, 'This method returns an iterator over the elements in this list in proper sequence.', '2023-03-26 11:06:20'),
	(119, 9, 10, 'This method returns the index of the last occurrence of the specified element in this list.\n\n', '2023-03-26 11:06:20'),
	(120, 16, 19, 'This method replaces the element at the specified position in this list with the specified element.\n\n', '2023-03-26 11:06:20'),
	(121, 16, 12, 'The operations that manipulate elements in the ArrayList are slow.', '2023-03-26 11:06:20'),
	(122, 14, 14, 'Retains from this list all of its elements that are contained in the specified collection.\n\n', '2023-03-26 11:06:20'),
	(123, 13, 14, 'The third overloaded constructor for the ArrayList class takes an already existing.', '2023-03-26 11:06:20'),
	(124, 11, 14, 'Once the ArrayList is created, there are multiple ways to initialize the ArrayList with values.\n\n', '2023-03-26 11:06:20'),
	(125, 17, 10, 'Here, you can pass an Array converted to List using the asList method of Arrays.\n\n', '2023-03-26 11:06:20'),
	(126, 18, 10, 'Here we use the anonymous inner class to initialize the ArrayList to values.', '2023-03-26 11:06:20'),
	(127, 19, 10, 'This method is used to initialize the ArrayList with the same values. \n\n', '2023-03-26 11:06:20'),
	(128, 20, 12, 'After that, in JavaScript, we\'ll look at how constructors and the prototype chain relate.', '2023-03-26 11:06:20'),
	(129, 20, 22, 'Object-oriented programming is about modeling a system as a collection of objects, where each object.', '2023-03-26 11:06:20'),
	(130, 20, 9, 'When we model a problem in terms of objects in OOP, we create abstract definitions.', '2023-03-26 11:06:20'),
	(131, 7, 22, 'Every professor has some properties in common: they all have a name and a subject.', '2023-03-26 11:06:20'),
	(132, 18, 22, 'n its own, a class doesn\'t do anything: it\'s a kind of template for creating concrete objects of that type. ', '2023-03-26 11:06:20'),
	(133, 11, 9, 'We pass values to the constructor for any internal state that we want to initialize in the new instance.', '2023-03-26 11:06:20'),
	(140, 17, 3, 'Plain Old Java Object The name is used to emphasize that a given object is an ordinary Java Object', '2023-03-29 00:00:00'),
	(141, 17, 3, 'new I would be interested to know how you would make this responsive if you had a few more columns or where the data is a little longer.', '2023-03-29 00:00:00'),
	(145, 17, 4, 'I would be interested again', '2023-03-29 00:00:00'),
	(150, 3, 31, 'Unit testing is a WhiteBox testing technique that is usually performed by the developer.', '2023-03-31 10:04:55'),
	(151, 4, 32, 'Though, in a practical world due to time crunch or reluctance of developers to tests.', '2023-03-31 10:04:55'),
	(152, 7, 33, 'Java is a high-level', '2023-03-31 10:04:57'),
	(153, 8, 34, 'Okay, so this is the only bit of mandatory theory.\n', '2023-03-31 10:04:59'),
	(154, 9, 35, 'HTML is the language in which most websites are written.', '2023-03-31 10:04:55'),
	(155, 10, 36, 'Tags and attributes are the basis of HTML.', '2023-03-31 10:04:55'),
	(156, 12, 37, 'Attributes contain additional pieces of information.', '2023-03-31 10:04:55');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Dumping data for table team_forum_6.phone_numbers: ~4 rows (approximately)
/*!40000 ALTER TABLE `phone_numbers` DISABLE KEYS */;
INSERT INTO `phone_numbers` (`phone_number_id`, `phone_number`, `user_id`) VALUES
	(5, '0326598225', 16),
	(6, '0365958545', 14),
	(7, '0365896445', 13),
	(8, '0356971221', 11);
/*!40000 ALTER TABLE `phone_numbers` ENABLE KEYS */;

-- Dumping data for table team_forum_6.posts: ~24 rows (approximately)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`post_id`, `user_id`, `title`, `content`, `creation_date`) VALUES
	(3, 9, 'Preventing mySQL injection with spring', 'I would be interested to know how you would make this responsive if you had a few more columns or where the data is a little longer.', '2023-03-29 10:55:14'),
	(4, 3, 'What is object-oriented programming?', 'OOP focuses on the objects that developers want to manipulate rather than the logic required to manipulate them. This approach to programming is well-suited for programs that are large', '2023-03-29 10:55:14'),
	(7, 3, 'What is polymorphism and why it is important?', 'NEW Polymorphism is one of the core concepts of object-oriented programming (OOP) and describes situations in which something occurs in several different forms. In computer science', '2023-03-29 00:00:00'),
	(9, 17, 'Question about Alpha java program in Telerik', 'Do you know how what i can learn there and how long i have to prepare for the exams?', '2023-03-25 03:04:00'),
	(10, 17, 'What is Static Method in Java', 'A static method is a method that belongs to a class rather than an instance of a class. This means you can call a static method without creating an object of the class. Static methods are sometimes called class methods.There are several reasons why you might want to use static methods. First, static methods can be called without creating an object of the class, which can be convenient if you only need to call the method once or if you don\'t need to store any data in an object after calling the method.', '2023-03-26 10:42:10'),
	(11, 15, 'How unit tests work and where i have to put them in my code?', 'In computer programming, unit testing is a software testing method by which individual units of source code—sets of one or more computer program modules together with associated control data, usage procedures, and operating procedures—are tested to determine whether they are fit for use', '2023-03-26 10:42:10'),
	(12, 16, 'In computer programming why it is important to write unit tests?', 'Unit testing is a software testing method by which individual units of source code—sets of one or more computer program modules together with associated control data', '2023-03-29 10:54:55'),
	(13, 21, 'What is Stack? Why is it useful for developers?', 'Stack is a linear data structure that follows a particular order in which the operations are performed. The order may be LIFO(Last In First Out) or FILO(First In Last Out). LIFO implies that the element that is inserted last, comes out first and FILO implies that the element that is inserted first, comes out last.', '2023-03-26 10:42:10'),
	(14, 20, 'Pop Operation in stack? How it works?', 'Accessing the content while removing it from the stack, is known as a Pop Operation. In an array implementation of pop() operation, the data element is not actually removed, instead top is decremented to a lower position in the stack to point to the next value. But in linked-list implementation, pop() actually removes data element and deallocates memory space.', '2023-03-26 10:42:10'),
	(15, 18, 'Queue? Could you share the difference between queue and stack?', 'A collection designed for holding elements prior to processing. Besides basic Collection operations, queues provide additional insertion, extraction, and inspection operations. Each of these methods exists in two forms: one throws an exception if the operation fails, the other returns a special value (either null or false, depending on the operation). The latter form of the insert operation is designed specifically for use with capacity-restricted Queue implementations; in most implementations, insert operations cannot fail.', '2023-03-26 10:42:10'),
	(16, 16, 'Implementation of the Queue Interface', 'In queues, elements are stored and accessed in First In, First Out manner. That is, elements are added from the behind and removed from the front.The Queue interface includes all the methods of the Collection interface. It is because Collection is the super interface of Queue.', '2023-03-26 10:42:10'),
	(17, 10, 'Some factors are considered for choosing the data structure?', 'What type of data needs to be stored?: It might be a possibility that a certain data structure can be the best fit for some kind of data.Cost of operations: If we want to minimize the cost for the operations for the most frequently performed operations. For example, we have a simple list on which we have to perform the search operation; then, we can create an array in which elements are stored in sorted order to perform the binary search. The binary search works very fast for the simple list as it divides the search space into half.', '2023-03-26 10:42:10'),
	(18, 10, 'Root of tree in Java, could you tell me more?', 'Root: The root node is the topmost node in the tree hierarchy. In other words, the root node is the one that doesn\'t have any parent. In the above structure, node numbered 1 is the root node of the tree. If a node is directly linked to some other node, it would be called a parent-child relationship.', '2023-03-26 10:42:10'),
	(19, 8, 'Java Collections – Interface', 'It is up to each collection to determine its own synchronization policy. In the absence of a stronger guarantee by the implementation, undefined behavior may result from the invocation of any method on a collection that is being mutated by another thread; this includes direct invocations, passing the collection to a method that might perform invocations, and using an existing iterator to examine the collection.', '2023-03-29 10:54:59'),
	(20, 7, 'Trees in Java: How to Implement a Binary Tree?', 'For the implementation, there’s an auxiliary Node class that will store int values and keeps a reference to each child. The first step is to find the place where we want to add a new node in order to keep the tree sorted. We’ll follow these rules starting from the root node:\n\nif the new node’s value is lower than the current node’s, go to the left child\nif the new node’s value is greater than the current node’s, go to the right child\nwhen the current node is null, we’ve reached a leaf node, we insert the new node in that position', '2023-03-26 10:42:10'),
	(21, 6, 'What is a Binary Tree?', 'A Tree is a non-linear data structure where data objects are generally organized in terms of hierarchical relationship. The structure is non-linear in the sense that, unlike Arrays, Linked Lists, Stack and Queues, data in a tree is not organized linearly. A binary tree is a recursive tree data structure where each node can have 2 children at most. ', '2023-03-26 10:42:10'),
	(22, 21, 'The ArrayList class is a resizable array', 'The difference between a built-in array and an ArrayList in Java, is that the size of an array cannot be modified (if you want to add or remove elements to/from an array, you have to create a new one). While elements can be added and removed from an ArrayList whenever you want. ', '2023-03-26 10:42:10'),
	(31, 2, 'C# (C-Sharp) is a programming language developed by Microsoft th', 'C# has roots from the C family, and the language is close to other popular languages like C++ and Java.\n\n', '2023-03-31 09:57:01'),
	(32, 6, 'JavaScript (JS) is a lightweight, interpreted, or just-in-time.', 'Answers some fundamental questions such as "what is JavaScript?", "what does it look like?", and "what can it do?", along with discussing key JavaScript features such as variables, strings, numbers, and arrays.', '2023-03-31 09:57:01'),
	(33, 7, 'HTML is the language in which most websites are written. ', 'A Markup Language is a way that computers speak to each other to control how text is processed and presented. To do this HTML uses two things: tags and attributes.', '2023-03-31 09:57:01'),
	(34, 4, 'Once you understand the fundamentals of HTML', 'We recommend that you learn further HTML and CSS at the same time. moving back and forth between the two topics. This is because HTML is far more interesting and much more fun to learn when you apply CSS, and you can\'t learn CSS without knowing HTML.', '2023-03-31 09:57:01'),
	(35, 8, 'CSS (Cascading Style Sheets) is used to style and layout web.', 'For example, to alter the font, color, size, and spacing of your content, split it into multiple columns, or add animations and other decorative features. This module provides a gentle beginning to your path towards CSS mastery with the basics of how it works, what the syntax looks like, and how you can start using it to add styling to HTML.', '2023-03-31 09:57:01'),
	(36, 25, 'Java was originally developed by James Gosling at Sun .', 'It was released in May 1995 as a core component of Sun Microsystems\' Java platform. The original and reference implementation Java compilers', '2023-03-31 09:57:01'),
	(37, 24, 'Unit Testing is a type of software testing where individual.', ' The purpose is to validate that each unit of the software code performs as expected. Unit Testing is done during the development (coding phase) of an application by the developers. Unit Tests isolate a section of code and verify its correctness. A unit may be an individual function, method, procedure, module, or object.', '2023-03-31 09:57:01');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Dumping data for table team_forum_6.post_like: ~18 rows (approximately)
/*!40000 ALTER TABLE `post_like` DISABLE KEYS */;
INSERT INTO `post_like` (`post_like_id`, `user_id`, `post_id`) VALUES
	(6, 2, 4),
	(7, 3, 4),
	(8, 4, 4),
	(12, 9, 11),
	(13, 10, 11),
	(14, 17, 18),
	(15, 18, 18),
	(16, 19, 17),
	(17, 20, 17),
	(18, 21, 9),
	(21, 11, 15),
	(22, 2, 16),
	(23, 7, 22),
	(24, 6, 19),
	(25, 17, 20),
	(26, 17, 4),
	(28, 17, 3),
	(29, 11, 3);
/*!40000 ALTER TABLE `post_like` ENABLE KEYS */;

-- Dumping data for table team_forum_6.post_tag: ~52 rows (approximately)
/*!40000 ALTER TABLE `post_tag` DISABLE KEYS */;
INSERT INTO `post_tag` (`post_tags_id`, `post_id`, `tag_id`) VALUES
	(5, 4, 16),
	(7, 21, 14),
	(8, 20, 14),
	(9, 11, 8),
	(10, 12, 11),
	(11, 16, 9),
	(12, 16, 11),
	(13, 9, 4),
	(15, 14, 13),
	(16, 15, 3),
	(17, 16, 2),
	(18, 21, 6),
	(19, 7, 1),
	(20, 7, 4),
	(21, 3, 4),
	(22, 3, 17),
	(23, 4, 17),
	(24, 7, 17),
	(25, 9, 17),
	(26, 16, 17),
	(27, 17, 18),
	(28, 18, 18),
	(29, 19, 18),
	(30, 20, 18),
	(31, 21, 18),
	(32, 22, 19),
	(33, 31, 19),
	(34, 32, 19),
	(35, 33, 19),
	(36, 34, 19),
	(37, 35, 20),
	(38, 36, 20),
	(39, 37, 20),
	(40, 22, 20),
	(41, 31, 20),
	(42, 32, 20),
	(43, 3, 21),
	(44, 4, 21),
	(45, 7, 21),
	(46, 9, 21),
	(47, 11, 21),
	(48, 12, 21),
	(49, 13, 22),
	(50, 14, 22),
	(51, 15, 23),
	(52, 16, 23),
	(53, 17, 24),
	(54, 33, 24),
	(55, 34, 25),
	(56, 10, 24),
	(57, 10, 11),
	(58, 10, 17);
/*!40000 ALTER TABLE `post_tag` ENABLE KEYS */;

-- Dumping data for table team_forum_6.tags: ~26 rows (approximately)
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` (`tag_id`, `title`) VALUES
	(1, 'advanced'),
	(13, 'alpha'),
	(5, 'array'),
	(8, 'arraylist'),
	(18, 'c#'),
	(25, 'css'),
	(10, 'databases'),
	(21, 'dsa'),
	(11, 'hql'),
	(24, 'html'),
	(17, 'java'),
	(4, 'javascript'),
	(19, 'junior'),
	(9, 'linkedlist'),
	(20, 'middle'),
	(16, 'oop'),
	(15, 'polymorphism'),
	(12, 'pyhton'),
	(7, 'queue'),
	(2, 'spring'),
	(3, 'sql'),
	(6, 'stack'),
	(22, 'telerik'),
	(14, 'tree'),
	(26, 'unit-testing'),
	(23, 'web');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;

-- Dumping data for table team_forum_6.users: ~23 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `user_name`, `password`, `first_name`, `last_name`, `email`, `user_role_id`, `profile_photo`, `creation_date`) VALUES
	(2, 'dev_user', '123456ivan', 'Ivan', 'Aleksandrov', 'ivan_aleksandrov@gmail.com', 1, 'Default_avatar', '2023-02-17 00:00:00'),
	(3, 'new_user', '256325256', 'Kaloyan', 'Georgiev', 'kaloyan_geo@abv.bg', 2, 'Default_avatar', '2023-02-17 10:37:26'),
	(4, 'javahost', 'host1236', 'Georgi', 'Atanasov', 'georgi_java@gmail.com', 2, 'Default_avatar', '2023-02-17 10:37:26'),
	(6, 'andev.ko', 'andrey1256', 'Andrey', 'Kostov', 'andy_0526@mail.bg', 4, 'Default_avatar', '2023-03-25 00:00:00'),
	(7, 'myspace', 'leo156244', 'Petar', 'Petrov', 'pesho_4525@gmail.com', 2, 'Default_avatar', '2023-02-17 10:37:26'),
	(8, 'orange', 'o35622555', 'Samuil', 'Blagoev', 'samuil_bl@gmail.com', 2, 'Default_avatar', '2023-02-17 10:37:26'),
	(9, 'java_user', 'k02255222', 'Viktor', 'Atanasov', 'viktor_1990@gmail.com', 2, 'Default_avatar', '2023-02-17 10:37:26'),
	(10, 'buddy56', 'buddy1365', 'Veronika', 'Mileva', 'nika_56@abv.bg', 2, 'Default_avatar', '2023-02-17 10:37:26'),
	(11, 'shanys', 'shantel1256', 'Adriana', 'Simeonova', 'adi_sim@gmail.com', 1, 'Default_avatar', '2023-02-17 10:37:26'),
	(12, 'reymond', '356955225', 'Rangel', 'Asenov', 'randy_90@mail.bg', 2, 'Default_avatar', '2023-02-17 10:37:26'),
	(13, 'jasondev', 'jason1256', 'Teodor', 'Nikolov', 'teo.nik@gmail.com', 1, 'Default_avatar', '2023-02-17 10:37:26'),
	(14, 'mynewforum', 'pass2561445', 'Tsvetan', 'Kostadinov', 'ceco.568@gmail.com', 1, 'Default_avatar', '2023-02-17 10:37:26'),
	(15, 'summer_code', 'sea1236542', 'Kalina', 'Petkova', 'kalinka@abv.g', 4, 'Default_avatar', '2023-02-17 10:37:26'),
	(16, 'leo_dev', 'ivanov15622', 'Leonard', 'Ivanov', 'leo_dev@abv.bg', 2, 'Default_avatar', '2023-02-17 00:00:00'),
	(17, 'new_dev_user', '112233', 'Ivancho', 'Ivanov', 'ivan@ivanov.bg', 2, 'Default_avatar', '2023-03-25 00:00:00'),
	(18, 'any_one', '56896352', 'Anton', 'Kaloyanov', 'kaloyan@adns.abv.bg', 2, 'Default_avatar', '2023-03-23 00:00:00'),
	(19, 'how_young', 'kakakakal', 'Ivanina', 'Petrova', 'iva_doll@k@abv.bg', 2, 'Default_avatar', '2023-03-26 10:26:29'),
	(20, 'very_good', '1234589624', 'Marian ', 'Kirilov', 'marian.kirilov.56@ak.bg', 2, 'Default_avatar', '2023-03-26 10:26:29'),
	(21, 'jessy_yo', '0055005500', 'Jessica', 'Ivanova', 'Jessy-iva@glail.com', 2, 'Default_avatar', '2023-03-26 10:26:29'),
	(24, 'andy_devko', '555556666', 'Andrey', 'Simeonov', 'dnk_52!mail.com', 2, 'Default_avatar', '2023-03-31 09:40:45'),
	(25, 'kori_to', '666669999', 'Kaloyan', 'Ivanov', 'kaloyan@mail.com', 2, 'Default_avatar', '2023-03-31 09:40:45'),
	(26, 'ivay_lo', 'ko56565656', 'Ivaylo', 'Nikolov', 'ivo_nik@abv.com', 2, 'Default_avatar', '2023-03-31 09:40:45'),
	(27, 'drago_dev', 'dr12563225', 'Drago', 'Marinov', 'drago@marinov.bg', 2, 'Default_avatar', '2023-03-31 09:40:45');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping data for table team_forum_6.user_role: ~3 rows (approximately)
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` (`user_role_id`, `user_role_name`) VALUES
	(1, 'admin'),
	(4, 'blocked'),
	(2, 'user');
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
