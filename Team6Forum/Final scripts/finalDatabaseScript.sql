create or replace table tags
(
    tag_id int auto_increment
        primary key,
    title  varchar(64) not null,
    constraint title
        unique (title)
);

create or replace table user_role
(
    user_role_id   int auto_increment
        primary key,
    user_role_name varchar(32) not null,
    constraint user_type_pk2
        unique (user_role_name)
);

create or replace table users
(
    user_id       int auto_increment
        primary key,
    user_name     varchar(32)                             not null,
    password      varchar(255)                            not null,
    first_name    varchar(32)                             not null,
    last_name     varchar(32)                             not null,
    email         varchar(255)                            not null,
    user_role_id  int                                     not null,
    profile_photo varchar(32) default 'Default_avatar'    null,
    creation_date datetime    default current_timestamp() not null,
    constraint users_pk2
        unique (user_name),
    constraint users_pk3
        unique (email),
    constraint users_user_type__id_fk
        foreign key (user_role_id) references user_role (user_role_id)
            on delete cascade
);

create or replace table phone_numbers
(
    phone_number_id int auto_increment
        primary key,
    phone_number    varchar(20) not null,
    user_id         int         null,
    constraint phone_numbers_pk
        unique (phone_number),
    constraint phone_numbers_user_id_fk
        foreign key (user_id) references users (user_id)
            on delete cascade
);

create or replace table posts
(
    post_id       int auto_increment
        primary key,
    user_id       int                                  null,
    title         varchar(64)                          not null,
    content       varchar(8192)                        not null,
    creation_date datetime default current_timestamp() null,
    constraint posts_user_id_fk
        foreign key (user_id) references users (user_id)
            on delete cascade
);

create or replace table comments
(
    comment_id    int auto_increment
        primary key,
    user_id       int                                  null,
    post_id       int                                  null,
    content       varchar(255)                         not null,
    creation_date datetime default current_timestamp() null,
    constraint comments_post_id_fk
        foreign key (post_id) references posts (post_id)
            on delete cascade,
    constraint comments_user_id_fk
        foreign key (user_id) references users (user_id)
            on delete cascade
);

create or replace table post_like
(
    post_like_id int auto_increment
        primary key,
    user_id      int null,
    post_id      int null,
    constraint posts_likes_post_id_fk
        foreign key (post_id) references posts (post_id)
            on delete cascade,
    constraint posts_likes_user_id_fk
        foreign key (user_id) references users (user_id)
            on delete cascade
);

create or replace table post_tag
(
    post_tags_id int auto_increment
        primary key,
    post_id      int null,
    tag_id       int null,
    constraint post_tags_post_id_fk
        foreign key (post_id) references posts (post_id)
            on delete cascade,
    constraint post_tags_tag_id_fk
        foreign key (tag_id) references tags (tag_id)
            on delete cascade
);

