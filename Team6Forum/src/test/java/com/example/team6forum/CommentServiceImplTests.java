package com.example.team6forum;

import com.example.team6forum.exceptions.UnauthorizedException;
import com.example.team6forum.models.Comment;
import com.example.team6forum.models.User;
import com.example.team6forum.repositories.contracts.CommentRepository;
import com.example.team6forum.services.CommentServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.example.team6forum.Helpers.createMockActiveAdmin;
import static com.example.team6forum.Helpers.createMockComment;

@ExtendWith(MockitoExtension.class)
public class CommentServiceImplTests {

    @Mock
    CommentRepository mockRepository;

    @InjectMocks
    CommentServiceImpl service;

    @Test
    public void create_Should_CallRepository_When_CommentWithSameNameDoesNotExist() {
        // Arrange
        Comment mockComment = createMockComment();


        // Act
        service.createComment(mockComment, Helpers.createMockActiveRegularUser());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).createComment(mockComment);
    }

    @Test
    public void update_Should_ThrowException_When_UserIsNotAuthor(){
        Comment mockComment = createMockComment();

        Assertions.assertThrows(UnauthorizedException.class,
                () -> service.update(mockComment, Mockito.mock(User.class)));
    }

    @Test
    public void update_Should_CallRepository_When_UserIsAuthor(){
        Comment mockComment = createMockComment();

        service.update(mockComment, mockComment.getCreatedBy());

        Mockito.verify(mockRepository, Mockito.times(1)).update(mockComment);
    }


    @Test
    @DisplayName("Delete should throw exception when user is not author")
    public void delete_Should_ThrowException_When_UserIsNotAuthor(){
        Comment mockComment = createMockComment();

        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockComment);

        Assertions.assertThrows(UnauthorizedException.class,
                () -> service.delete(mockComment.getCommentId(), Mockito.mock(User.class)));
    }

    @Test
    @DisplayName("Delete should delete comment when user is author")
    public void delete_Should_CallRepository_When_UserIsAuthor(){
        Comment mockComment = createMockComment();
        User mockUser = mockComment.getCreatedBy();

        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockComment);

        service.delete(mockComment.getCommentId(), mockUser);

        Mockito.verify(mockRepository, Mockito.times(1)).delete(mockComment.getCommentId());
    }

    @Test
    @DisplayName("Delete should delete comment when user is admin")
    public void delete_Should_CallRepository_When_UserIsAdmin(){
        Comment mockComment = createMockComment();
        User mockUser = createMockActiveAdmin();

        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockComment);

        service.delete(mockComment.getCommentId(), mockUser);

        Mockito.verify(mockRepository, Mockito.times(1)).delete(mockComment.getCommentId());
    }

}
