package com.example.team6forum;
import com.example.team6forum.models.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class Helpers {

    public static User createMockActiveAdmin() {
        var mockUser = new User();
        mockUser.setUserId(5);
        mockUser.setFirstName("Ivan");
        mockUser.setLastName("Ivanov");
        mockUser.setUserName("dev_user");
        mockUser.setPassword("dev_1235!");
        mockUser.setEmail("penda35@forum.com");
        mockUser.setUserRole(createMockUserRoleAdmin());
        mockUser.setCreationDate(LocalDate.now());
        return mockUser;
    }

    public static User createMockActiveRegularUser() {
        var mockUser = new User();
        mockUser.setUserId(1);
        mockUser.setFirstName("Todor");
        mockUser.setLastName("Ivanov");
        mockUser.setUserName("new_user");
        mockUser.setPassword("dev_8965");
        mockUser.setEmail("penda@forum.com");
        mockUser.setUserRole(createMockUserRoleUserRegularUser());
        mockUser.setCreationDate(LocalDate.now());
        return mockUser;
    }
    public static User createAnotherMockActiveRegularUser() {
        var mockUser = new User();
        mockUser.setUserId(2);
        mockUser.setFirstName("Todor2");
        mockUser.setLastName("Ivanov2");
        mockUser.setUserName("new_user2");
        mockUser.setPassword("2dev_8965");
        mockUser.setEmail("2penda@forum.com");
        mockUser.setUserRole(createMockUserRoleUserRegularUser());
        mockUser.setCreationDate(LocalDate.now());
        return mockUser;
    }

    public static User createMockBlockedUser() {
        var mockUser = new User();
        mockUser.setUserId(1);
        mockUser.setFirstName("Andrey");
        mockUser.setLastName("Ivanov");
        mockUser.setUserName("user_forum");
        mockUser.setPassword("dev_kak");
        mockUser.setEmail("penda@forum.com");
        mockUser.setUserRole(createMockUserRoleUserBlockedUser());
        mockUser.setCreationDate(LocalDate.now());
        return mockUser;
    }

    public static UserRole createMockUserRoleAdmin(){
        var mockUserRole = new UserRole();
        mockUserRole.setUserRoleId(1);
        mockUserRole.setUserRoleName("admin");
        return mockUserRole;
    }
    public static UserRole createMockUserRoleUserRegularUser(){
        var mockUserRole = new UserRole();
        mockUserRole.setUserRoleId(2);
        mockUserRole.setUserRoleName("user");
        return mockUserRole;
    }

    public static UserRole createMockUserRoleUserBlockedUser(){
        var mockUserRole = new UserRole();
        mockUserRole.setUserRoleId(4);
        mockUserRole.setUserRoleName("blocked_user");
        return mockUserRole;
    }

    public static Comment createMockComment() {
        var mockComment = new Comment();
        mockComment.setCommentId(1);
        mockComment.setContent("MockComment");
        mockComment.setCreationDate(LocalDate.now());
        mockComment.setCreatedBy(createMockActiveRegularUser());
        return mockComment;
    }

    public static PhoneNumber createMockNumber(){
        PhoneNumber mockPhoneNumber = new PhoneNumber();
        mockPhoneNumber.setPhoneNumberId(1);
        mockPhoneNumber.setPhoneNumber("0888888888");
        mockPhoneNumber.setUser(createMockActiveAdmin());
        return mockPhoneNumber;
    }

    public static Post createMockPost(){
        var mockPost = new Post();
        mockPost.setPostId(3);
        mockPost.setTitle("Some random post title....");
        mockPost.setContent("Some mock post content...");
        //mockPost.setCreationDate(LocalDateTime.of(2000,12,10, 2, 45));
        /*mockPost.setCreationDate(LocalDateTime.now());*/
        User regularUser = createMockActiveRegularUser();
        mockPost.setUser(regularUser);
        HashSet<User> usersSet = new HashSet<>(Set.of(regularUser));
        mockPost.setPostLikers(usersSet);
        return mockPost;
    }

    public static User createMockUser(){
        var mockUser = new User();
        mockUser.setUserId(3);
        mockUser.setFirstName("Mock");
        mockUser.setLastName("User");
        mockUser.setUserName("mock_user");
        mockUser.setPassword("mock_1235!");
        mockUser.setEmail("mockuser@gmail.com");
        mockUser.setUserRole(createMockUserRoleUserRegularUser());
        mockUser.setCreationDate(LocalDate.now());
        return mockUser;
    }
}

