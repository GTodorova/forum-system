package com.example.team6forum;

import com.example.team6forum.exceptions.DuplicateEntityException;
import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.exceptions.UnauthorizedException;
import com.example.team6forum.models.Post;
import com.example.team6forum.models.User;
import com.example.team6forum.repositories.contracts.PostRepository;
import com.example.team6forum.services.PostServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.example.team6forum.Helpers.*;
import static com.example.team6forum.Helpers.createMockPost;

@ExtendWith(MockitoExtension.class)
public class PostServiceTests {
    @Mock
    PostRepository mockRepository;
    @InjectMocks
    PostServiceImpl postService;

    @Test
    public void getPosts_Should_CallRepository() {
        Optional<String> myOptional = Optional.of("someString");
        // Arrange
        Mockito.when(mockRepository.getAll(myOptional))
                .thenReturn(null);
        // Act
        postService.getAll(myOptional);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAll(myOptional);
    }

    @Test
    public void get_Should_ReturnPost_When_MatchByIdExist() {
        // Arrange
        Post mockPost = createMockPost();

        Mockito.when(mockRepository.get(Mockito.anyInt()))
                .thenReturn(mockPost);

        // Act
        Post result = postService.get(mockPost.getPostId());

        // Assert
        Assertions.assertEquals(mockPost, result);
    }
    @Test
    public void create_Should_Throw_When_NameAlreadyExists() {
        // Arrange
        Post existingPost = createMockPost();
        Post postToCreate = createMockPost();
        postToCreate.setTitle(existingPost.getTitle());

        User mockNewUser = Helpers.createMockActiveRegularUser();

        Mockito.when(mockRepository.get(postToCreate.getTitle()))
                .thenReturn(existingPost);

        // Act and Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> postService.create(postToCreate, mockNewUser));
    }

    @Test
    public void create_Should_Throw_When_PostWithSameTitleExist(){
        Post mockPost = createMockPost();
        User mockUser = createMockActiveRegularUser();

        Mockito.when(mockRepository.get(mockPost.getTitle()))
                .thenReturn(mockPost);

        Assertions.assertThrows(DuplicateEntityException.class, () -> postService.create(mockPost, mockUser));
    }

    @Test
    public void create_Should_CallRepository_When_PostWithSameTitleDoesNotExist() {
        // Arrange
        Post mockPost = createMockPost();
        User mockUser = createMockActiveRegularUser();

        Mockito.when(mockRepository.get(mockPost.getTitle()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        postService.create(mockPost, mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).create(mockPost);
    }

    @Test
    public void create_Should_Throw_When_UserIsBlocked() {
        // Arrange
        Post mockPost = createMockPost();
        User mockUser = createMockBlockedUser();

        // Act and Assert
        Assertions.assertThrows(UnauthorizedException.class, () -> postService.create(mockPost, mockUser));
    }

    @Test
    public void update_Should_CallRepository_When_UserIsCreator() {
        Post mockPost = createMockPost();
        User mockUserCreator = mockPost.getUser();

        Mockito.when(mockRepository.get(Mockito.anyInt()))
                .thenReturn(mockPost);

        Mockito.when(mockRepository.get(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        postService.update(mockPost, mockUserCreator);

        Mockito.verify(mockRepository, Mockito.times(1)).update(mockPost);
    }

    @Test
    public void update_Should_Throw_When_UserIsBlocked() {
        Post mockPost = createMockPost();
        User mockUserBlocked = mockPost.getUser();
        mockUserBlocked.setUserRole(createMockUserRoleUserBlockedUser());

        Mockito.when(mockRepository.get(Mockito.anyInt()))
                .thenReturn(mockPost);

        Assertions.assertThrows(UnauthorizedException.class,
                () -> postService.update(mockPost, mockUserBlocked));

    }

    @Test
    public void update_Should_ThrowException_When_UserIsNotCreatorOrAdmin(){
        Post mockPost = createMockPost();

        Mockito.when(mockRepository.get(mockPost.getPostId()))
                .thenReturn(mockPost);

        Assertions.assertThrows(
                UnauthorizedException.class,
                () -> postService.update(mockPost,Mockito.mock(User.class)));
    }

    @Test
    public void update_Should_ThrowException_When_PostTitleIsTaken(){
        Post mockPost = createMockPost();
        User mockUserCreator = mockPost.getUser();

        Mockito.when(mockRepository.get(mockPost.getPostId()))
                .thenReturn(mockPost);

        Post mockExistingPostWithSameTitle = createMockPost();
        mockExistingPostWithSameTitle.setPostId(2);

        Mockito.when(mockRepository.get(mockPost.getTitle()))
                .thenReturn(mockExistingPostWithSameTitle);

        Assertions.assertThrows(
                DuplicateEntityException.class,
                () -> postService.update(mockPost, mockUserCreator));
    }

    @Test
    public void delete_Should_CallRepository_When_UserIsCreator() {
        Post mockPost = createMockPost();
        User mockUserCreator = mockPost.getUser();

        Mockito.when(mockRepository.get(Mockito.anyInt()))
                .thenReturn(mockPost);

        postService.delete(mockPost.getPostId(), mockUserCreator);

        Mockito.verify(mockRepository, Mockito.times(1)).delete(mockPost.getPostId());
    }

    @Test
    public void delete_Should_ThrowException_When_UserIsNotCreatorOrAdmin(){
        Post mockPost = createMockPost();

        Mockito.when(mockRepository.get(Mockito.anyInt()))
                .thenReturn(mockPost);

        Assertions.assertThrows(
                UnauthorizedException.class,
                () -> postService.delete(mockPost.getPostId(),Mockito.mock(User.class)));
    }

    @Test
    public void createLike_Should_ThrowException_When_UserLikedPost() {
        Post mockPost = createMockPost();
        User mockUser = mockPost.getUser();

        Assertions.assertThrows(DuplicateEntityException.class, () -> postService.createLike(mockUser, mockPost));
    }

    @Test
    public void createLike_Should_CallRepository_When_UserDidNotLikePost() {
        Post mockPost = createMockPost();
        User mockUser = createMockUser();

        postService.createLike(mockUser, mockPost);

        Assertions.assertEquals(2, mockPost.getPostLikers().size());
    }

    @Test
    public void deleteLike_Should_UpdateRepository_When_UserDislikePost(){
        Post mockPost = createMockPost();
        User mockUser = mockPost.getUser();

        postService.deleteLike(mockPost, mockUser);

        Assertions.assertEquals(0, mockPost.getPostLikers().size());
    }
}
