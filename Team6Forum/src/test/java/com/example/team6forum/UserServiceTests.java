package com.example.team6forum;

import com.example.team6forum.exceptions.DuplicateEntityException;
import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.exceptions.UnauthorizedException;
import com.example.team6forum.models.User;
import com.example.team6forum.models.UserFilterOptions;
import com.example.team6forum.models.UserRole;
import com.example.team6forum.repositories.contracts.UserRepository;
import com.example.team6forum.services.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock
    UserRepository mockRepository;
    @InjectMocks
    UserServiceImpl userService;



    @Test
    public void getAllUsers_Should_CallRepository() {
        // Arrange
        Mockito.when(mockRepository.getAllUsers())
                .thenReturn(new ArrayList<>());
        // Act
        userService.getAllUsers();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAllUsers();
    }
    @Test
    public void getById_Should_ReturnUser_When_Match_Exist(){
        User mockUser = Helpers.createMockActiveRegularUser();
        Mockito.when(mockRepository.getUserById(mockUser.getUserId())).thenReturn(mockUser);
        User result = userService.getUserById(mockUser.getUserId());
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    void createUser_should_throwException_when_usernameIsDuplicate() {
        User mockExistingUser = Helpers.createMockActiveRegularUser();
        User mockNewUser = Helpers.createMockActiveRegularUser();
        mockNewUser.setUserName(mockExistingUser.getUserName());
        Mockito.when(mockRepository.getUserByUsername(mockExistingUser.getUserName())).thenReturn(mockExistingUser);
        Assertions.assertThrows(DuplicateEntityException.class, () -> userService.createUser(mockNewUser));
    }

    @Test
    void createUser_should_callRepositoryCreateMethod_when_usernameAndEmailUnique() {
        User mockUser = Helpers.createMockActiveRegularUser();

        Mockito.when(mockRepository.getUserByUsername(mockUser.getUserName())).thenThrow(EntityNotFoundException.class);
        Mockito.when(mockRepository.getUserByEmail(mockUser.getEmail())).thenThrow(EntityNotFoundException.class);
        userService.createUser(mockUser);

        Mockito.verify(mockRepository, Mockito.times(1)).createUser(mockUser);
    }

    @Test
    void testCreateUserWithDuplicateUsername() {
        User mockUser = Helpers.createMockActiveRegularUser();
        User mockUserNew = Helpers.createMockActiveAdmin();
        mockUserNew.setUserName(mockUser.getUserName());
        Mockito.when(mockRepository.getUserByUsername(mockUser.getUserName())).thenReturn(mockUser);
        Assertions.assertThrows(DuplicateEntityException.class, () -> userService.createUser(mockUser));
    }

    @Test
    void createUser_should_throwException_when_emailIsDuplicate() {
        User mockExistingUser = Helpers.createMockActiveRegularUser();
        User mockNewUser = Helpers.createAnotherMockActiveRegularUser();
        mockNewUser.setEmail(mockExistingUser.getEmail());
        Mockito.when(mockRepository.getUserByUsername(mockNewUser.getUserName())).thenThrow(EntityNotFoundException.class);
        Mockito.when(mockRepository.getUserByEmail(mockExistingUser.getEmail())).thenReturn(mockExistingUser);

        Assertions.assertThrows(DuplicateEntityException.class, () -> userService.createUser(mockNewUser));

    }

    @Test
    void emailIsTaken_should_returnFalse_when_sameEmailFound() {
        User mockUser = Helpers.createMockActiveRegularUser();
        Mockito.when(mockRepository.getUserByEmail(mockUser.getEmail())).thenReturn(Helpers.createMockActiveRegularUser());
        boolean result = userService.emailIsTaken(mockUser);
        Assertions.assertFalse(result);
    }

    @Test
    void testEmailIsTaken_should_returnTrue_when_NoSuchEmailFound() {
        User mockUser = Helpers.createMockActiveRegularUser();
        User mockNewUser = Helpers.createMockActiveAdmin();
        mockNewUser.setEmail(mockUser.getEmail());
        Mockito.when(mockRepository.getUserByEmail(mockUser.getEmail())).thenReturn(mockNewUser);
        boolean emailIsTaken = userService.emailIsTaken(mockUser);
        Assertions.assertTrue(emailIsTaken);
    }


@Test
void updateUser_should_throwException_when_initiatedByBlockedAdmin() {
    User initiator = Helpers.createMockBlockedUser();
    initiator.setUserRole(new UserRole(4));
    User userToUpd = Helpers.createAnotherMockActiveRegularUser();

    Assertions.assertThrows(UnauthorizedException.class, () -> userService.updateUser(initiator, userToUpd));
}

    @Test
    void updateUser_should_throwException_when_idDoesNotExist() {
        User initiator = Helpers.createMockActiveAdmin();
        initiator.setUserRole(new UserRole(1));
        User mockUser = Helpers.createMockActiveRegularUser();
        Mockito.when(mockRepository.getUserById(mockUser.getUserId())).thenThrow(
               EntityNotFoundException.class);

        //Act,Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> userService.updateUser(initiator, mockUser));
    }
    @Test
    public void update_Should_ThrowUserNameChangeException_WhenUserTriesToChangeUsername(){
        var mockUser = Helpers.createMockActiveRegularUser();
        var mockUserTwo =Helpers.createMockBlockedUser();
        mockUserTwo.setUserName(mockUser.getUserName() + "yo");

        Assertions.assertThrows(UnauthorizedException.class, () -> userService.updateUser(mockUser,mockUserTwo));
    }


    @Test
    void userNameIsTaken_should_returnFalse_when_sameEmailFound() {
        User mockUser = Helpers.createMockActiveRegularUser();
        Mockito.when(mockRepository.getUserByUsername(mockUser.getUserName())).thenReturn(Helpers.createMockActiveRegularUser());
        boolean result = userService.usernameIsTaken(mockUser);
        Assertions.assertFalse(result);
    }

    @Test
    void userNameIsTaken_should_returnTrue_when_NoSuchEmailFound() {
        User mockUser = Helpers.createMockActiveRegularUser();
        User mockNewUser = Helpers.createMockActiveAdmin();
        mockNewUser.setEmail(mockUser.getUserName());
        Mockito.when(mockRepository.getUserByUsername(mockUser.getUserName())).thenReturn(mockNewUser);
        boolean result = userService.usernameIsTaken(mockUser);
        Assertions.assertTrue(result);
    }


    @Test
    void getUserByName_should_returnMatchingUser_when_matchExists() {
        User mockUser = Helpers.createMockActiveRegularUser();
        Mockito.when(userService.getUserByUsername(mockUser.getUserName())).thenReturn(mockUser);
        User result = userService.getUserByUsername(mockUser.getUserName());
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    void getAllRoles_should_callRepository(){
        userService.getAllRoles();
        Mockito.verify(mockRepository,Mockito.times(1)).getAllRoles();
    }


    @Test
    public void deleteUser_Should_ThrowUnauthorizedException_WhenUserNotAdmin(){
        var mockUser = Helpers.createMockActiveRegularUser();
        var mockUserToDelete = Helpers.createMockActiveRegularUser();
        mockUserToDelete.setUserId(2);
        mockUser.setUserRole(new UserRole(2));

        Assertions.assertThrows(UnauthorizedException.class, () -> userService.deleteUser(mockUser, 2));
    }

    @Test
    public void deleteUser_shouldCall_RepositoryWhen_UserWantsToDeleteHisAccount(){
        var mockUser = Helpers.createMockActiveRegularUser();

        userService.deleteUser(mockUser, 1);

        Mockito.verify(mockRepository, Mockito.times(1)).deleteUser(1);
    }
    @Test
    public void deleteUser_shouldCall_RepositoryWhen_AdminWantsToDeleteIt(){
        var mockUser = Helpers.createMockActiveRegularUser();
        mockUser.setUserId(3);
        var admin = Helpers.createMockActiveAdmin();
        admin.setUserRole(new UserRole(1));
        admin.setUserId(3);

        userService.deleteUser(admin, 3);

        Mockito.verify(mockRepository, Mockito.times(1)).deleteUser(3);
    }

}



