package com.example.team6forum;
import com.example.team6forum.exceptions.DuplicateEntityException;
import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.exceptions.UnauthorizedException;
import com.example.team6forum.models.PhoneNumber;
import com.example.team6forum.models.User;
import com.example.team6forum.models.UserRole;
import com.example.team6forum.repositories.contracts.PhoneRepository;
import com.example.team6forum.services.PhoneNumberServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.example.team6forum.Helpers.createMockActiveAdmin;

@ExtendWith(MockitoExtension.class)
public class PhoneNumberServiceTests {

    @Mock
    PhoneRepository mockRepository;
    @InjectMocks
    PhoneNumberServiceImpl phoneNumberService;


    @Test
    public void getAll_Should_CallRepository() {

        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());
        phoneNumberService.getAll();
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_ReturnPhoneNumber_When_Match_Exist(){
        PhoneNumber mockPhoneNumber = Helpers.createMockNumber();
        Mockito.when(mockRepository.getById(mockPhoneNumber.getPhoneNumberId())).thenReturn(mockPhoneNumber);
        PhoneNumber result = phoneNumberService.getById(mockPhoneNumber.getPhoneNumberId());
        Assertions.assertEquals(mockPhoneNumber, result);
    }

    @Test
    public void create_Should_CallRepository_When_PhoneNumberDoesNotExistAndUserIsAdmin() {
        PhoneNumber mockPhoneNumber = Helpers.createMockNumber();
        User user = createMockActiveAdmin();
        user.setUserRole(new UserRole(1));
        Mockito.when(mockRepository.getByNumber(mockPhoneNumber.getPhoneNumber())).thenThrow(EntityNotFoundException.class);
        phoneNumberService.create(mockPhoneNumber, user);
        Mockito.verify(mockRepository, Mockito.times(1)).create(mockPhoneNumber);
    }

    @Test
    void update_should_throwException_when_initiatedByRegularUser() {
        User initiator = Helpers.createMockActiveRegularUser();
        PhoneNumber mockPhoneNumber = Helpers.createMockNumber();
        User user = createMockActiveAdmin();
        user.setUserRole(new UserRole(1));
        Assertions.assertThrows(UnauthorizedException.class, () -> phoneNumberService.update(mockPhoneNumber, initiator));
    }

    @Test
    void update_should_throwException_when_phoneNumberAlreadyExist() {
        User initiator = Helpers.createMockActiveAdmin();
        PhoneNumber mockPhoneNumber = Helpers.createMockNumber();
        PhoneNumber phoneNumber = Helpers.createMockNumber();
        phoneNumber.setPhoneNumberId(2);
        Mockito.when(mockRepository.getByNumber(mockPhoneNumber.getPhoneNumber())).thenReturn(phoneNumber);
        Assertions.assertThrows(DuplicateEntityException.class, () -> phoneNumberService.update(mockPhoneNumber, initiator));
    }

    @Test
    public void deletePhoneNumber_shouldCall_RepositoryWhen_AdminWantsToDeleteIt(){
        var admin = Helpers.createMockActiveAdmin();
        admin.setUserRole(new UserRole(1));
        admin.setUserId(3);

        PhoneNumber phoneNumber = Helpers.createMockNumber();

        phoneNumberService.delete(1, admin);

        Mockito.verify(mockRepository, Mockito.times(1)).delete(1);
    }

    @Test
    public void deletePhoneNumber_Should_ThrowUnauthorizedException_WhenUserNotAdmin(){
        var mockUser = Helpers.createMockActiveRegularUser();
        PhoneNumber phoneNumber = Helpers.createMockNumber();
        Assertions.assertThrows(UnauthorizedException.class, () -> phoneNumberService.delete(phoneNumber.getPhoneNumberId(), mockUser));
    }


}
