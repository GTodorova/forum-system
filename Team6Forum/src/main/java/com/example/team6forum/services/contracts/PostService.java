package com.example.team6forum.services.contracts;

import com.example.team6forum.models.Post;
import com.example.team6forum.models.PostFilterOptions;
import com.example.team6forum.models.Tag;
import com.example.team6forum.models.User;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface PostService {
    List<Post> filter(Optional<String> username, Optional<String> content, Optional<LocalDate> startDate,
                      Optional<LocalDate> endDate, Optional<String> postName,
                      Optional<Integer> tagId,
                      Optional<String> sortBy, Optional<String> sortOrder);

    List<Post> getAll(Optional<String> search);

    List<Post> getAllPostsFilter(PostFilterOptions postFilterOptions);

    List<Post> getAllPosts();

    List<Tag> getTags();

    Post get(int id);

    void create(Post post, User user);

    void addTagToPost(User user, int id, Post post);

    void update(Post post, User user);

    void delete(int id, User user);

    long getNumberOfPosts();

    List<Post> top10newest();

    List<Post> top10mostCommented();


    void createLike(User user, Post post);

    void deleteLike(Post post, User user);

    List<Post> searchByTagName(Optional<String> tagName, User user);

}
