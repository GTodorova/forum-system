package com.example.team6forum.services;

import com.example.team6forum.exceptions.DuplicateEntityException;
import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.exceptions.UnauthorizedException;
import com.example.team6forum.models.PhoneNumber;
import com.example.team6forum.models.User;
import com.example.team6forum.repositories.contracts.PhoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PhoneNumberServiceImpl implements com.example.team6forum.services.contracts.PhoneNumberService {

    private final PhoneRepository repository;

    @Autowired
    public PhoneNumberServiceImpl(PhoneRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<PhoneNumber> getAll() {
        return repository.getAll().stream().filter
                (phoneNumber -> phoneNumber.getUser().isAdmin()).collect(Collectors.toList());
    }

    @Override
    public PhoneNumber getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void create(PhoneNumber phoneNumber, User user) {
        boolean duplicateExist = true;

        if (!user.isAdmin()) {
            throw new UnauthorizedException("Only admins can create numbers");
        }
        if (!phoneNumber.getUser().isAdmin()) {
            throw new UnauthorizedException("Only admins can have numbers");
        }
        try {
            repository.getByNumber(phoneNumber.getPhoneNumber());
        } catch (EntityNotFoundException e) {
            duplicateExist = false;
        }
        if (duplicateExist) {
            throw new DuplicateEntityException("Number", "number", phoneNumber.getPhoneNumber());
        }
        repository.create(phoneNumber);
    }

    @Override
    public void update(PhoneNumber number, User user) {

        boolean duplicateExist = true;

        if (!user.isAdmin()) {
            throw new UnauthorizedException("Only admins can modify numbers");
        }

        try {
            PhoneNumber existingPhoneNumber = repository.getByNumber(number.getPhoneNumber());
            if (existingPhoneNumber.getPhoneNumberId() == number.getPhoneNumberId()) {
                duplicateExist = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExist = false;
        }
        if (duplicateExist) {
            throw new DuplicateEntityException("Phone number", "number", number.getPhoneNumber());
        }
        repository.update(number);
    }

    @Override
    public void delete(int id, User user) {
        if (!user.isAdmin()) {
            throw new UnauthorizedException("Only admins can delete comments.");
        }
        repository.delete(id);
    }

}
