package com.example.team6forum.services;

import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.exceptions.NoEntityFoundException;
import com.example.team6forum.exceptions.UnauthorizedException;
import com.example.team6forum.models.Comment;
import com.example.team6forum.models.CommentFilterOptions;
import com.example.team6forum.models.User;
import com.example.team6forum.repositories.contracts.CommentRepository;
import com.example.team6forum.services.contracts.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Component
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository repository) {
        this.commentRepository = repository;
    }

    @Override
    public List<Comment> getAll(CommentFilterOptions commentFilterOptions) {
        return
                commentRepository.getComments(commentFilterOptions);
    }

    @Override
    public List<Comment> getAllComments() {
        return commentRepository.getAllComments();
    }

    @Override
    public int getNumberOfComments(int postId) {
        if (commentRepository.getNumberOfComments(postId) == 0) {
            throw new NoEntityFoundException("Comments", "Post", postId);
        }
        return commentRepository.getNumberOfComments(postId);
    }

    @Override
    public List<Comment> getCommentsByUserId(int userId) {
        if (commentRepository.getCommentsByUserId(userId).isEmpty()) {
            throw new EntityNotFoundException("Comment", "this", "author");
        }
        return commentRepository.getCommentsByUserId(userId);
    }

    @Override
    public List<Comment> getCommentsByPostId(int postId) {
        return commentRepository.getCommentsByPostId(postId);
    }

    @Override
    public List<Comment> filter(Optional<String> username, Optional<String> content, Optional<LocalDate> startDate,
                                Optional<LocalDate> endDate, Optional<String> postName,
                                Optional<String> sortBy, Optional<String> sortOrder) {
        return commentRepository.filter(username, content, startDate, endDate, postName, sortBy, sortOrder);
    }

    @Override
    public Comment getById(int id) {
        return commentRepository.getById(id);
    }


    @Override
    public void createComment(Comment comment, User user) {
        if (user.isBlocked()) {
            throw new UnauthorizedException("Blocked users can not add comments");
        }
        commentRepository.createComment(comment);
    }

    @Override
    public void update(Comment comment, User user) {
        if (comment.getCreatedBy().getUserId() != user.getUserId()) {
            throw new UnauthorizedException("Only author can update comment.");
        }
        commentRepository.update(comment);
    }

    @Override
    public void delete(int id, User user) {
        Comment comment = commentRepository.getById(id);
        if (user.isAdmin() || comment.getCreatedBy().equals(user)) {
            commentRepository.delete(id);
        } else {
            throw new UnauthorizedException("Only admins and owners can delete comments.");
        }
    }
}
