package com.example.team6forum.services;

import com.example.team6forum.exceptions.DuplicateEntityException;
import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.exceptions.UnauthorizedException;
import com.example.team6forum.models.Post;
import com.example.team6forum.models.PostFilterOptions;
import com.example.team6forum.models.Tag;
import com.example.team6forum.models.User;
import com.example.team6forum.repositories.contracts.PostRepository;
import com.example.team6forum.services.contracts.PostService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class PostServiceImpl implements PostService {
    public static final String MODIFY_POST_ERROR_MESSAGE = "Only admins or post creator can modify posts.";
    private final PostRepository postRepository;

    public PostServiceImpl(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public Post get(int id) {
        return postRepository.get(id);
    }

    @Override
    public void create(Post post, User user) {
        boolean duplicatePostExists = true;
        try {
            postRepository.get(post.getTitle());
        } catch (EntityNotFoundException e) {
            duplicatePostExists = false;
        }
        if (user.isBlocked()) {
            throw new UnauthorizedException("Blocked users can not create posts");
        }

        if (duplicatePostExists) {
            throw new DuplicateEntityException("Post", "title", post.getTitle());
        }

        post.setUser(user);
        post.setCreationDate(LocalDate.now());
        postRepository.create(post);
    }

    @Override
    public void update(Post post, User user) {
        checkModifyPermissions(post.getPostId(), user);

        if (post.getTags() != null) {
            modifyTags(post);
        }

        boolean duplicatePostExists = true;
        try {
            Post existingPost = postRepository.get(post.getTitle());
            if (existingPost.getPostId() == post.getPostId()) {
                duplicatePostExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicatePostExists = false;
        }

        //TODO User owner change the post title
        if (duplicatePostExists && post.getUser().equals(user)) {
            throw new DuplicateEntityException("Post", "id", post.getTitle());
        }

        post.setCreationDate(postRepository.get(post.getPostId()).getCreationDate());
        postRepository.update(post);
    }

    public void addTagToPost(User user, int id, Post post) {
        checkModifyPermissions(id, user);
        modifyTags(post);
        postRepository.update(post);
    }

    private void modifyTags(Post post) {
        Set<Tag> tagsFromUser = post.getTags();
        Set<Tag> tagsModified = new HashSet<>();

        for (Tag tag : tagsFromUser) {
            try {
                tagsModified.add(postRepository.getTagByName(tag.getTitle()));
            } catch (EntityNotFoundException e) {
                tagsModified.add(tag);
            }
        }
        post.setTags(tagsModified);
    }

    @Override
    public void delete(int id, User user) {
        checkModifyPermissions(id, user);

        Post postFromRepository = postRepository.get(id);
        if(postFromRepository.getTags() != null) {
            postFromRepository.getTags().clear();
            postRepository.update(postFromRepository);
        }

        postRepository.delete(id);
    }

    @Override
    public long getNumberOfPosts() {
        return postRepository.getNumberOfPosts();
    }

    @Override
    public List<Post> top10newest() {
        return postRepository.top10newest();
    }

    @Override
    public List<Post> top10mostCommented() {
        return postRepository.mostCommented();
    }

    @Override
    public void createLike(User user, Post post) {
        Set<User> currentLikers = post.getPostLikers();

        if (currentLikers.contains(user)) {
            throw new DuplicateEntityException("User", "already liked this post", "-");
        }

        // TODO removed else
            currentLikers.add(user);
            postRepository.update(post);

    }

    @Override
    public void deleteLike(Post post, User user) {
        Set<User> currentLikers = post.getPostLikers();

        if (!currentLikers.contains(user)) {
            throw new DuplicateEntityException("User", "have not liked this post", "-");
        }

        // TODO removed else
            currentLikers.remove(user);
            postRepository.update(post);

    }

    @Override
    public List<Post> searchByTagName(Optional<String> tagName, User user) {
        return postRepository.searchByTagName(tagName);
    }

    @Override
    public List<Post> filter(Optional<String> username, Optional<String> content, Optional<LocalDate> startDate,
                             Optional<LocalDate> endDate, Optional<String> postName,
                             Optional<Integer> tagId,
                             Optional<String> sortBy, Optional<String> sortOrder) {
        return postRepository.filter(username, content, startDate, endDate, postName, tagId, sortBy, sortOrder);
    }

    @Override
    public List<Post> getAll(Optional<String> search) {
        return postRepository.getAll(search);
    }

    @Override
    public List<Post> getAllPostsFilter(PostFilterOptions postFilterOptions) {
        return postRepository.getAllPostsFilter(postFilterOptions);
    }

    @Override
    public List<Post> getAllPosts() {
        return postRepository.getAllPosts();
    }

    @Override
    public List<Tag> getTags() {
        return postRepository.getTags();
    }


    private void checkModifyPermissions(int postId, User user) {
        Post post = postRepository.get(postId);
        if (!(user.isAdmin() || post.getUser().equals(user)) || user.isBlocked()) {
            throw new UnauthorizedException(MODIFY_POST_ERROR_MESSAGE);
        }
    }
}
