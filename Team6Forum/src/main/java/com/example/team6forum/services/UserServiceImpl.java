package com.example.team6forum.services;

import com.example.team6forum.exceptions.DuplicateEntityException;
import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.exceptions.UnauthorizedException;
import com.example.team6forum.models.User;
import com.example.team6forum.models.UserFilterOptions;
import com.example.team6forum.models.UserRole;
import com.example.team6forum.repositories.contracts.UserRepository;
import com.example.team6forum.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    public static final String UNAUTHORIZED_ERROR_MESSEGE = "Only admins and owners can modify users";
    public static final String CHANGING_USER_ROLE_ERROR = "Only admins can change user role";
    private final UserRepository repository;

    @Autowired
    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    private static void changeRolePermission(User currentUser, User userToUpdate) {
        /*if (currentUser.getUserRole().getUserRoleId() != userToUpdate.getUserRole().getUserRoleId() && !currentUser.isAdmin()) {
            throw new UnauthorizedException(CHANGING_USER_ROLE_ERROR);
        }*/

        if((currentUser.getUserRole().getUserRoleId() != userToUpdate.getUserRole().getUserRoleId()) && !currentUser.isAdmin()) {
            throw new UnauthorizedException(CHANGING_USER_ROLE_ERROR);
        }
    }

    @Override
    public List<User> getAllUsers() {
        return repository.getAllUsers();
    }

    @Override
    public User getUserById(int id) {
        return repository.getUserById(id);
    }

    @Override
    public void createUser(User user) {
        checkUsernameForDuplicates(user);
        checkMailForDuplicates(user);
        repository.createUser(user);
    }

    public void checkUsernameForDuplicates(User user) {
        boolean duplicateUsernameExists = true;
        try {
            repository.getUserByUsername(user.getUserName());
        } catch (EntityNotFoundException e) {
            duplicateUsernameExists = false;
        }
        if (duplicateUsernameExists) {
            throw new DuplicateEntityException("User", "username", user.getUserName());
        }
    }

    public void checkMailForDuplicates(User user) {
        boolean duplicateUsernameExists = true;
        try {
            repository.getUserByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateUsernameExists = false;
        }

        if (duplicateUsernameExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }
    }

    @Override
    public void updateUser(User currentUser, User userToUpdate) {
        existingUserCheck(userToUpdate);
        checkModifyPermissions(currentUser, userToUpdate);
        usernameIsTaken(userToUpdate);
        if (usernameIsTaken(userToUpdate)) {
            throw new DuplicateEntityException("Username", "username", userToUpdate.getUserName());
        }
        if (emailIsTaken(userToUpdate)) {
            throw new DuplicateEntityException("Email", "email", userToUpdate.getEmail());
        }
        userToUpdate.setCreationDate(repository.getUserById(userToUpdate.getUserId()).getCreationDate());
        repository.updateUser(userToUpdate);
    }

    public boolean emailIsTaken(User userToUpdate) {
        try {
            User existingUser = repository.getUserByEmail(userToUpdate.getEmail());
            if (existingUser.getUserId() == userToUpdate.getUserId()) {
                return false;
            }
        } catch (EntityNotFoundException e) {
            return false;
        }
        return true;
    }

    public boolean usernameIsTaken(User userToUpdate) {
        try {
            User existingUser = repository.getUserByUsername(userToUpdate.getUserName());
            if (existingUser.getUserId() == userToUpdate.getUserId()) {
                return false;
            }
        } catch (EntityNotFoundException e) {
            return false;
        }
        return true;
    }

    public void checkModifyPermissions(User currentUser, User userToUpdate) {
        if (!currentUser.isAdmin() && currentUser.getUserId() != userToUpdate.getUserId()) {
            throw new UnauthorizedException(UNAUTHORIZED_ERROR_MESSEGE);
        }
        changeRolePermission(currentUser, userToUpdate);
    }

    public void existingUserCheck(User userToUpdate) {
        try {
            User existingUser = repository.getUserById(userToUpdate.getUserId());
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("User Id", "Id", Integer.toString(userToUpdate.getUserId()));
        }
    }

    @Override
    public void deleteUser(User currentUser, int userToDeleteID) {
        if (!currentUser.isAdmin() && currentUser.getUserId() != userToDeleteID) {
            throw new UnauthorizedException("Only admins and owners can delete users");
        }
        repository.deleteUser(userToDeleteID);
    }

    @Override
    public User getUserByUsername(String username) {
        return repository.getUserByUsername(username);
    }

    @Override
    public List<User> filter(Optional<String> username, Optional<String> firstName, Optional<String> lastName,
                             Optional<LocalDate> creationDate, Optional<Integer> userType,
                             Optional<String> sortBy, Optional<String> sortOrder) {
        return repository.filter(username, firstName, lastName, creationDate, userType, sortBy, sortOrder);
    }

    @Override
    public List<UserRole> getAllRoles() {
        return repository.getAllRoles();
    }

    @Override
    public List<User> getAllUsers(UserFilterOptions userFilterOptions) {
        return repository.getAllUsers(userFilterOptions);
    }
}
