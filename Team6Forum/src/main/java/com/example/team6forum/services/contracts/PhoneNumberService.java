package com.example.team6forum.services.contracts;

import com.example.team6forum.models.PhoneNumber;
import com.example.team6forum.models.User;

import java.util.List;

public interface PhoneNumberService {
    List<PhoneNumber> getAll();

    PhoneNumber getById(int id);

    void create(PhoneNumber phoneNumber, User user);

    void update(PhoneNumber number, User user);

    void delete(int id, User user);
}
