package com.example.team6forum.services.contracts;

import com.example.team6forum.models.Comment;
import com.example.team6forum.models.CommentFilterOptions;
import com.example.team6forum.models.User;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface CommentService {

    List<Comment> getAll(CommentFilterOptions commentFilterOptions);

    List<Comment> getAllComments();

    int getNumberOfComments(int postId);

    List<Comment> getCommentsByUserId(int userId);

    List<Comment> getCommentsByPostId(int postId);

    List<Comment> filter(Optional<String> username, Optional<String> content, Optional<LocalDate> startDate,
                         Optional<LocalDate> endDate, Optional<String> postName,
                         Optional<String> sortBy, Optional<String> sortOrder);

    Comment getById(int id);

    void createComment(Comment comment, User user);

    void update(Comment comment, User user);

    void delete(int id, User user);
}
