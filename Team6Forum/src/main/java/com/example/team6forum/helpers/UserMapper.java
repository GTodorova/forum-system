package com.example.team6forum.helpers;

import com.example.team6forum.models.RegisterDto;
import com.example.team6forum.models.User;
import com.example.team6forum.models.UserInDto;
import com.example.team6forum.models.UserRole;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class UserMapper {
    public User dtoToUser(int id, UserInDto userInDto) {
        User user = dtoToUser(userInDto);
        user.setUserId(id);

        return user;
    }

    public User dtoToUser(UserInDto userInDto) {
        User user = new User();
        user.setUserName(userInDto.getUserName());
        user.setPassword(userInDto.getPassword());
        user.setFirstName(userInDto.getFirstName());
        user.setLastName(userInDto.getLastName());
        user.setEmail(userInDto.getEmail());
        user.setCreationDate(LocalDate.now());
        UserRole userRole = new UserRole();
        userRole.setUserRoleId(userInDto.getUserTypeId());
        user.setUserRole(userRole);
        return user;
    }

    public User fromDtoToUserRegister(RegisterDto registerDto) {
        User user = new User();
        user.setUserName(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setEmail(registerDto.getEmail());
        UserRole basicUser = new UserRole(2);
        user.setUserRole(basicUser);
        return user;
    }

    public UserInDto userToInDto(User user) {
        UserInDto userInDto = new UserInDto();
        userInDto.setUserName(user.getUserName());
        userInDto.setPassword(user.getPassword());
        userInDto.setFirstName(user.getFirstName());
        userInDto.setLastName(user.getLastName());
        userInDto.setEmail(user.getEmail());
        userInDto.setUserTypeId(user.getUserRoleId());
        return userInDto;
    }

}
