package com.example.team6forum.helpers;

import com.example.team6forum.models.Comment;
import com.example.team6forum.models.CommentCreateDto;
import com.example.team6forum.models.User;
import com.example.team6forum.services.contracts.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommentMapper {

    private final PostService postService;

    @Autowired
    public CommentMapper(PostService postService) {
        this.postService = postService;
    }

    public Comment dtoCreateToObject(CommentCreateDto commentDto, User user, int id) {
        Comment comment = new Comment();
        comment.setContent(commentDto.getContent());
        comment.setPost(postService.get(id));
        comment.setCreatedBy(user);
        return comment;
    }

    public Comment dtoUpdateToObject(Comment comment, CommentCreateDto commentDto,int userId) {
        comment.setContent(commentDto.getContent());
        return comment;
    }

    public CommentCreateDto toDto(Comment comment) {
        CommentCreateDto commentCreateDto = new CommentCreateDto();
        commentCreateDto.setContent(comment.getContent());
        return commentCreateDto;
    }
}
