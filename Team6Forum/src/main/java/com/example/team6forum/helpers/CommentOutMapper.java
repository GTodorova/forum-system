package com.example.team6forum.helpers;

import com.example.team6forum.models.Comment;
import com.example.team6forum.models.CommentOutDTO;
import com.example.team6forum.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CommentOutMapper {

    @Autowired
    public CommentOutMapper() {
    }

    public CommentOutDTO commentToOutDto(Comment comment, User user) {
        CommentOutDTO commentOutDTO = new CommentOutDTO();
        commentOutDTO.setCommentId(comment.getCommentId());
        commentOutDTO.setContent(comment.getContent());
        commentOutDTO.setUsername(user.getUserName());
        commentOutDTO.setCreationDate(comment.getCreationDate());
        commentOutDTO.setUserId(user.getUserId());
        return commentOutDTO;
    }

    public List<CommentOutDTO> commentOutDTOList(List<Comment> comments) {
        List<CommentOutDTO> commentOutDTOList = new ArrayList<>();
        comments.forEach(comment -> commentOutDTOList.add(commentToOutDto(comment, comment.getCreatedBy())));
        return commentOutDTOList;
    }

}
