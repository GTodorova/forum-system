package com.example.team6forum.helpers;

import com.example.team6forum.models.User;
import com.example.team6forum.models.UserOutDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class UserOutMapper {
    public UserOutDto userToOutDto(User user) {
        return new UserOutDto(user.getUserName(), user.getFirstName(),
                user.getLastName(), user.getEmail(), user.getUserRole().getUserRoleName());
    }

    public List<UserOutDto> userListToOutDtoList(List<User> users) {
        List<UserOutDto> UserOutDtos = new ArrayList<>();
        for (User user : users) {
            UserOutDtos.add(userToOutDto(user));
        }
        return UserOutDtos;
    }

    public Set<UserOutDto> userSetToOutDtoSet(Set<User> users) {
        Set<UserOutDto> UserOutDtos = new HashSet<>();
        for (User user : users) {
            UserOutDtos.add(userToOutDto(user));
        }
        return UserOutDtos;
    }
}
