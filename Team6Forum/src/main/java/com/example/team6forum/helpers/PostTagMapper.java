package com.example.team6forum.helpers;

import com.example.team6forum.models.Post;
import com.example.team6forum.models.PostTagDto;
import com.example.team6forum.models.Tag;
import com.example.team6forum.services.contracts.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class PostTagMapper {
    private final PostService service;

    @Autowired
    public PostTagMapper(PostService service) {
        this.service = service;
    }

    public Post dtoToObject(int postId, PostTagDto postTagDto) {
        Post post = service.get(postId);
        Set<Tag> tags = new HashSet<>();
        for (String tag : postTagDto.getTagNames()) {
            tags.add(new Tag(tag));
        }
        post.setTags(tags);
        return post;
    }
}
