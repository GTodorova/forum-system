package com.example.team6forum.helpers;

import com.example.team6forum.models.PhoneNumber;
import com.example.team6forum.models.PhoneNumberDto;
import com.example.team6forum.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PhoneNumberMapper {

    private final UserService userService;

    @Autowired
    public PhoneNumberMapper(UserService userService) {
        this.userService = userService;
    }

    public PhoneNumber dtoToObject(int id, PhoneNumberDto phoneNumberDto) {
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setPhoneNumberId(id);
        return dtoToObject(phoneNumber, phoneNumberDto);
    }

    public PhoneNumber dtoToObject(PhoneNumber phoneNumber, PhoneNumberDto phoneNumberDto) {
        phoneNumber.setPhoneNumber(phoneNumberDto.getPhoneNumber());
        phoneNumber.setUser(userService.getUserById(phoneNumberDto.getUserId()));
        return phoneNumber;
    }


}
