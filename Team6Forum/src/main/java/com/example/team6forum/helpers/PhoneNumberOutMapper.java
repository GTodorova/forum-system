package com.example.team6forum.helpers;

import com.example.team6forum.models.PhoneNumber;
import com.example.team6forum.models.PhoneNumberOutDto;
import com.example.team6forum.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PhoneNumberOutMapper {

    @Autowired
    public PhoneNumberOutMapper() {
    }

    public PhoneNumberOutDto phoneNumberOutDto(PhoneNumber phoneNumber, User user) {
        PhoneNumberOutDto phoneNumberOutDto = new PhoneNumberOutDto();
        phoneNumberOutDto.setPhoneNumber(phoneNumber.getPhoneNumber());
        phoneNumberOutDto.setFirstName(user.getFirstName());
        phoneNumberOutDto.setLastName(user.getLastName());
        return phoneNumberOutDto;
    }

    public List<PhoneNumberOutDto> phoneNumberOutDtoList(List<PhoneNumber> phoneNumbers) {
        List<PhoneNumberOutDto> phoneNumbersOutDTOList = new ArrayList<>();
        phoneNumbers.forEach(phoneNumber -> phoneNumbersOutDTOList.add(phoneNumberOutDto(phoneNumber, phoneNumber.getUser())));
        return phoneNumbersOutDTOList;
    }
}
