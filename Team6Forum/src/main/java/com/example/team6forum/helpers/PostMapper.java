package com.example.team6forum.helpers;

import com.example.team6forum.models.Post;
import com.example.team6forum.models.PostCreateDto;
import com.example.team6forum.models.PostDto;
import com.example.team6forum.models.User;
import com.example.team6forum.services.contracts.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PostMapper {

    private final PostService postService;

    @Autowired
    public PostMapper(PostService postService) {
        this.postService = postService;
    }

    public Post fromDto(int id, PostDto postDto) {
        Post post = fromDto(postDto);
        post.setPostId(id);
        Post repositoryPost = postService.get(id);
        post.setUser(repositoryPost.getUser());
        return post;
    }

    public Post fromDto(PostDto postDto) {
        Post post = new Post();
        post.setTitle(postDto.getTitle());
        post.setContent(postDto.getContent());
        post.setTags(postDto.getTags());
        return post;
    }

    public Post dtoCreateToObject(PostCreateDto postCreateDto, User user) {
        Post post = new Post();
        post.setContent(postCreateDto.getContent());
        post.setTitle(postCreateDto.getTitle());
        post.setUser(user);
        return post;
    }

    public PostDto toDto(Post post) {
        PostDto postDto = new PostDto();
        postDto.setTitle(post.getTitle());
        postDto.setContent(post.getContent());
        postDto.setTags(post.getTags());
        return postDto;
    }
}