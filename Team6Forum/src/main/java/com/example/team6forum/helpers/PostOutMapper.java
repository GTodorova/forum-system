package com.example.team6forum.helpers;

import com.example.team6forum.models.Post;
import com.example.team6forum.models.PostOutDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PostOutMapper {

    private final UserOutMapper userOutMapper;

    @Autowired
    public PostOutMapper(UserOutMapper userOutMapper) {
        this.userOutMapper = userOutMapper;
    }

    public PostOutDto postToOutDto(Post post) {
        PostOutDto postOutDto = new PostOutDto();
        postOutDto.setPostId(post.getPostId());
        postOutDto.setTitle(post.getTitle());
        postOutDto.setContent(post.getContent());
        postOutDto.setCreationDate(post.getCreationDate());
        postOutDto.setUser(userOutMapper.userToOutDto(post.getUser()));
        postOutDto.setTags(post.getTags());
        postOutDto.setPostLikers(userOutMapper.userSetToOutDtoSet(post.getPostLikers()));
        return postOutDto;
    }

    public List<PostOutDto> postToOutDto(List<Post> posts) {
        List<PostOutDto> postOutDto = new ArrayList<>();
        posts.forEach(post -> postOutDto.add(postToOutDto(post)));
        return postOutDto;
    }
}
