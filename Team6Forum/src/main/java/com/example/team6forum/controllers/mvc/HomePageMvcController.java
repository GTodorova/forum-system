package com.example.team6forum.controllers.mvc;

import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.helpers.PhoneNumberOutMapper;
import com.example.team6forum.models.Post;
import com.example.team6forum.models.User;
import com.example.team6forum.services.contracts.PhoneNumberService;
import com.example.team6forum.services.contracts.PostService;
import com.example.team6forum.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/")
public class HomePageMvcController {

    private final PostService postService;

    private final PhoneNumberService phoneNumberService;

    private final PhoneNumberOutMapper phoneNumberOutMapper;

    private final UserService userService;


    public HomePageMvcController(PostService postService, PhoneNumberService phoneNumberService, PhoneNumberOutMapper phoneNumberOutMapper, UserService userService) {
        this.postService = postService;
        this.phoneNumberService = phoneNumberService;
        this.phoneNumberOutMapper = phoneNumberOutMapper;
        this.userService = userService;
    }


    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserName")
    public String populateCurrentUserName(HttpSession session) {
        return (String) session.getAttribute("currentUser");
    }

    @ModelAttribute("isAdmin")
    public boolean isAdmin(HttpSession session) {
        User user;
        try {
            user = userService.getUserByUsername(populateCurrentUserName(session));
            return user.isAdmin();
        } catch (EntityNotFoundException e) {
            return false;
        }
    }

    @ModelAttribute("isBlockedUser")
    public boolean isBlocked(HttpSession session) {
        User user;
        try {
            user = userService.getUserByUsername(populateCurrentUserName(session));
            return user.isBlocked();
        } catch (EntityNotFoundException e) {
            return false;
        }
    }

    @GetMapping
    public String showHomePage(Model model) {

        List<Post> top10 = postService.top10newest();
        model.addAttribute("posts", top10);

        for (Post post : top10) {
            model.addAttribute("post", post);
        }

        model.addAttribute("numberPost", postService.getNumberOfPosts());
        model.addAttribute("numberOfUsers", userService.getAllUsers().size());
        return "index";
    }

    @GetMapping("/about")
    public String showAboutPage() {

        return "about";
    }

    @GetMapping("/contact")
    public String showContactPage(Model model) {
        model.addAttribute("users",userService.getAllUsers().stream().filter(User::isAdmin));
        return "contact";
    }

    @GetMapping("/mostCommented")
    public String mostCommentedPosts(Model model) {
        model.addAttribute("posts", postService.top10mostCommented());
        return "most-commented";
    }
}
