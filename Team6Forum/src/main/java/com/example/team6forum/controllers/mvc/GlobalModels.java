package com.example.team6forum.controllers.mvc;

import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.models.User;
import com.example.team6forum.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public class GlobalModels {

    private final UserService userService;

    @Autowired
    public GlobalModels(UserService userService) {
        this.userService = userService;
    }


    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserName")
    public String populateCurrentUserName(HttpSession session) {
        return (String) session.getAttribute("currentUser");
    }

    @ModelAttribute("userId")
    public int populateCurrentUserId(HttpSession session) {
        int currentUserId = 0;
        if (session.getAttribute("userId") != null) {
            currentUserId = (Integer) session.getAttribute("userId");
        }
        return currentUserId;
    }

    @ModelAttribute("isAdmin")
    public boolean isAdmin(HttpSession session) {
        User user;
        try {
            user = userService.getUserByUsername(populateCurrentUserName(session));
            return user.isAdmin();
        } catch (EntityNotFoundException e) {
            return false;
        }
    }

    @ModelAttribute("isBlockedUser")
    public boolean isBlocked(HttpSession session) {
        User user;
        try {
            user = userService.getUserByUsername(populateCurrentUserName(session));
            return user.isBlocked();
        } catch (EntityNotFoundException e) {
            return false;
        }
    }
}
