package com.example.team6forum.controllers.rest;

import com.example.team6forum.exceptions.DuplicateEntityException;
import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.exceptions.UnauthorizedException;
import com.example.team6forum.helpers.AuthenticationHelper;
import com.example.team6forum.helpers.PostMapper;
import com.example.team6forum.helpers.PostOutMapper;
import com.example.team6forum.helpers.PostTagMapper;
import com.example.team6forum.models.*;
import com.example.team6forum.services.contracts.PostService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/posts")
public class PostRestController {
    private final PostService service;
    private final PostMapper postMapper;
    private final AuthenticationHelper authenticationHelper;
    private final PostTagMapper postTagMapper;
    private final PostOutMapper postOutMapper;

    @Autowired
    public PostRestController(PostService service, PostMapper postMapper, AuthenticationHelper authenticationHelper,
                              PostTagMapper postTagMapper, PostOutMapper postOutMapper) {
        this.service = service;
        this.postMapper = postMapper;
        this.authenticationHelper = authenticationHelper;
        this.postTagMapper = postTagMapper;
        this.postOutMapper = postOutMapper;
    }

    @GetMapping
    public List<PostOutDto> getAll(@RequestParam(required = false) Optional<String> search) {
        List<Post> posts = service.getAll(search);
        return postOutMapper.postToOutDto(posts);
    }

    @GetMapping("/filter")
    public List<PostOutDto> filter(
            @RequestParam(required = false) String title,
            @RequestParam(required = false) String content,
            @RequestParam(required = false) String username,
            @RequestParam(required = false) LocalDate startDate,
            @RequestParam(required = false) LocalDate endDate,
            @RequestParam(required = false) Integer tagId,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortOrder) {
        PostFilterOptions postFilterOptions = new PostFilterOptions(title, content, username, startDate, endDate, tagId, sortBy, sortOrder);

        try {
            List<Post> posts = service.getAllPostsFilter(postFilterOptions);
            return postOutMapper.postToOutDto(posts);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public PostOutDto get(@PathVariable int id) {
        try {
            Post post = service.get(id);
            return postOutMapper.postToOutDto(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public PostOutDto create(@RequestHeader HttpHeaders headers, @Valid @RequestBody PostCreateDto postCreateDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postMapper.dtoCreateToObject(postCreateDto, user);
            service.create(post, user);
            return postOutMapper.postToOutDto(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public PostOutDto update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody PostDto postDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postMapper.fromDto(id, postDto);
            service.update(post, user);
            return postOutMapper.postToOutDto(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/tag-list")
    public void addToTagList(
            @RequestHeader HttpHeaders headers,
            @PathVariable int id,
            @RequestBody PostTagDto postTagDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postTagMapper.dtoToObject(id, postTagDto);
            service.addTagToPost(user, id, post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/searchByTagName")
    public List<PostOutDto> searchByTagName(@RequestHeader HttpHeaders headers, @RequestParam(required = false) Optional<String> tagName) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            List<Post> posts = service.searchByTagName(tagName, user);
            return postOutMapper.postToOutDto(posts);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You need to log in");
        }
    }


    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            service.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/top10newest")
    public List<PostOutDto> top10newest() {
        List<Post> posts = service.top10newest();
        return postOutMapper.postToOutDto(posts);
    }

    @GetMapping("/count")
    public long getNumberOfPosts() {
        return service.getNumberOfPosts();
    }

    @PostMapping("/{id}/likes")
    public void createLike(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = service.get(id);
            service.createLike(user, post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}/likes")
    public void deleteLike(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = service.get(id);
            service.deleteLike(post, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}