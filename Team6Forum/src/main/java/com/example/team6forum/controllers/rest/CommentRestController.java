package com.example.team6forum.controllers.rest;

import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.exceptions.UnauthorizedException;
import com.example.team6forum.helpers.AuthenticationHelper;
import com.example.team6forum.helpers.CommentMapper;
import com.example.team6forum.helpers.CommentOutMapper;
import com.example.team6forum.models.*;
import com.example.team6forum.services.contracts.CommentService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/comments")

public class CommentRestController {

    private final CommentService service;
    private final CommentMapper commentMapper;
    private final CommentOutMapper commentOutMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CommentRestController(CommentService service, CommentMapper commentMapper, CommentOutMapper commentOutMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.commentMapper = commentMapper;
        this.commentOutMapper = commentOutMapper;
        this.authenticationHelper = authenticationHelper;
    }


    @GetMapping
    public List<CommentOutDTO> getComments(
    ) {
        List<Comment> comments = service.getAllComments();
        List<CommentOutDTO> commentsOut = new ArrayList<>();
        for (Comment comment : comments) {
            commentsOut.add(commentOutMapper.commentToOutDto(comment, comment.getCreatedBy()));
        }
        return commentsOut;
    }


    @GetMapping("/filter")
    public List<CommentOutDTO> filter(
            @RequestParam(required = false) String username,
            @RequestParam(required = false) String postTitle,
            @RequestParam(required = false) String content,
            @RequestParam(required = false) LocalDate startDate,
            @RequestParam(required = false) LocalDate endDate,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortOrder) {
        CommentFilterOptions commentFilterOptions = new CommentFilterOptions(username, postTitle, content, startDate, endDate, sortBy, sortOrder);
        try {
            List<Comment> comments = service.getAll(commentFilterOptions);
            return commentOutMapper.commentOutDTOList(comments);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public CommentOutDTO get(@PathVariable int id) {
        try {
            return commentOutMapper.commentToOutDto(service.getById(id), service.getById(id).getCreatedBy());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/user/{userId}")
    public List<CommentOutDTO> getByUserId(@RequestHeader HttpHeaders headers, @PathVariable int userId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            List<Comment> comments = service.getCommentsByUserId(userId);
            List<CommentOutDTO> commentsDto = new ArrayList<>();
            for (Comment comment : comments) {
                commentsDto.add(commentOutMapper.commentToOutDto(comment, comment.getCreatedBy()));
            }
            return commentsDto;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @GetMapping("/post/{postId}")
    public List<CommentOutDTO> getByPostId(@RequestHeader HttpHeaders headers, @PathVariable int postId) {
        User user = authenticationHelper.tryGetUser(headers);
        List<Comment> comments = service.getCommentsByPostId(postId);
        List<CommentOutDTO> commentsDto = new ArrayList<>();
        for (Comment comment : comments) {
            commentsDto.add(commentOutMapper.commentToOutDto(comment, comment.getCreatedBy()));
        }
        return commentsDto;
    }


    @PostMapping("/post/{id}")
    public CommentOutDTO create(@PathVariable int id, @RequestHeader HttpHeaders headers, @Valid @RequestBody CommentCreateDto commentDto) {
        User user = authenticationHelper.tryGetUser(headers);
        Comment comment = commentMapper.dtoCreateToObject(commentDto, user, id);
        service.createComment(comment, user);
        return commentOutMapper.commentToOutDto(comment, user);
    }

    @DeleteMapping("/{id}")
    public String delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            service.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        return String.format("Comment with id %d was deleted.", id);
    }

    @PutMapping("/{id}")
    public CommentOutDTO update(@RequestHeader HttpHeaders headers, @PathVariable int id,
                          @Valid @RequestBody CommentCreateDto commentCreateDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Comment comment = commentMapper.dtoUpdateToObject(new Comment(), commentCreateDto, user.getUserId());
            service.update(comment, user);
            return commentOutMapper.commentToOutDto(comment, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
