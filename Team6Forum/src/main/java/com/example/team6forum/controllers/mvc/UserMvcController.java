package com.example.team6forum.controllers.mvc;

import com.example.team6forum.exceptions.DuplicateEntityException;
import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.exceptions.UnauthorizedException;
import com.example.team6forum.helpers.AuthenticationHelper;
import com.example.team6forum.helpers.UserMapper;
import com.example.team6forum.helpers.UserOutMapper;
import com.example.team6forum.models.*;
import com.example.team6forum.services.contracts.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/users")
public class UserMvcController {
    private final UserService userService;
    private final UserMapper userMapper;
    private final UserOutMapper userOutMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserMvcController(UserService userService,
                             UserMapper userMapper,
                             UserOutMapper userOutMapper,
                             AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.userOutMapper = userOutMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("userRoles")
    public List<UserRole> populateUserRoles() {
        return userService.getAllRoles();
    }

    @GetMapping("")
    public String showUsers(@ModelAttribute("UserFilterOptions") UserFilterDto userFilterDto,
                            Model model,
                            HttpSession session) {

        UserFilterOptions userFilterOptions = new UserFilterOptions(
                userFilterDto.getUsername(),
                userFilterDto.getFirstName(),
                userFilterDto.getLastName(),
                userFilterDto.getCreationDate(),
                userFilterDto.getUserType(),
                userFilterDto.getSortBy(),
                userFilterDto.getSortOrder()
        );

        try {
            User currentUser = authenticationHelper.tryGetCurrentUser(session);
            if (!currentUser.isAdmin()) {
                return "unauthorized";
            }
            model.addAttribute("users", userService.getAllUsers(userFilterOptions));
            model.addAttribute("userFilterOptions", userFilterOptions);
            return "UsersView";

        } catch (UnauthorizedException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{id}")
    public String showSingleUser(@PathVariable int id, Model model,
                                 HttpServletRequest request,
                                 HttpSession session) {
        try {
            User currentUser = authenticationHelper.tryGetCurrentUser(session);
            if ((currentUser.getUserId() != id && !currentUser.isAdmin())
                    || currentUser.isBlocked()) {
                model.addAttribute("error", "You are blocked");
                return "unauthorized";
            }
            ;
            User userInDB = userService.getUserById(id);
            model.addAttribute("user", userInDB);
            model.addAttribute("httpServletRequest", request);
            return "UserView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }
    }

    @GetMapping("/new")
    public String showNewUserPage(Model model, HttpServletRequest request, HttpSession session) {
        User curentUser;
        try {
            curentUser = authenticationHelper.tryGetCurrentUser(session);
        }
        catch (UnauthorizedException e){
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }
        if (!curentUser.isAdmin()) {
            model.addAttribute("error", "Unauthorized");
            return "unauthorized";
        }
        model.addAttribute("httpServletRequest", request);
        model.addAttribute("user", new UserInDto());
        return "UserCreateView";

    }

    @PostMapping("/new")
    public String createNewUser(@Valid @ModelAttribute("user") UserInDto userInDto,
                                BindingResult errors,
                                HttpServletRequest request,
                                Model model, HttpSession session) {

        model.addAttribute("httpServletRequest", request);
        if (errors.hasErrors()) {
            return "UserCreateView";
        }
        try {
            User curentUser = authenticationHelper.tryGetCurrentUser(session);
            if (!curentUser.isAdmin()) {
                model.addAttribute("error", "Unauthorized");
                return "unauthorized";
            }
            User user = userMapper.dtoToUser(userInDto);
            userService.createUser(user);
            return "redirect:/users";
        } catch (DuplicateEntityException e) {

            if (e.getMessage().contains("username")) {
                errors.rejectValue("userName", "duplicate_error", e.getMessage());
            } else if (e.getMessage().contains("email")) {
                errors.rejectValue("email", "duplicate_error", e.getMessage());
            }

            return "UserCreateView";
        }
    }

    @GetMapping("/{id}/update")
    public String showUpdateUserPage(@PathVariable int id, Model model,
                                     HttpServletRequest request, HttpSession session) {
        try {
            User curentUser = authenticationHelper.tryGetCurrentUser(session);
            if ((curentUser.getUserId() != id || curentUser.isBlocked()) && !curentUser.isAdmin()) {
                model.addAttribute("error", "Unauthorized");
                return "unauthorized";
            }
        } catch (UnauthorizedException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }

        model.addAttribute("httpServletRequest", request);
        try {
            User user = userService.getUserById(id);
            UserInDto userInDto = userMapper.userToInDto(user);
            model.addAttribute("user", userInDto);
            return "UpdateUserView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("user") UserInDto userInDto,
                             BindingResult bindingResult,
                             Model model,
                             HttpServletRequest request, HttpSession session) {

        model.addAttribute("httpServletRequest", request.getRequestURI());
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
            if ((currentUser.getUserId() != id || currentUser.isBlocked()) && !currentUser.isAdmin()) {
                model.addAttribute("error", "Unauthorized");
                return "unauthorized";
            }
        } catch (UnauthorizedException e) {
            return "unauthorized";
        }

        if (bindingResult.hasErrors()) {
            return "UpdateUserView";
        }

        try {
            User user = userMapper.dtoToUser(id, userInDto);
            userService.updateUser(currentUser, user);
            return "redirect:/users/{id}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "UpdateUserView";
        } catch (UnauthorizedException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }

        /*try {
            User curentUser = authenticationHelper.tryGetCurrentUser(session);
            if ((curentUser.getUserId() != id || curentUser.isBlocked()) && !curentUser.isAdmin()) {
                model.addAttribute("error", "Unauthorized");
                return "unauthorized";
            }

            User userToUpdate = userMapper.dtoToUser(id, userInDto);
            userService.updateUser(curentUser, userToUpdate);
            return "redirect:/users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("userName", "duplicate_username", e.getMessage());
            return "UpdateUserView";
        } catch (UnauthorizedException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }*/
    }

    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id, Model model, HttpSession session) {
        try {
            User curentUser = authenticationHelper.tryGetCurrentUser(session);
            if ((curentUser.getUserId() != id || curentUser.isBlocked()) && !curentUser.isAdmin()) {
                model.addAttribute("error", "Unauthorized");
                return "unauthorized";
            }
            userService.deleteUser(curentUser, id);
            if (curentUser.getUserId() == id) {
                session.invalidate();
            }

            return "redirect:/users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }
    }
}
