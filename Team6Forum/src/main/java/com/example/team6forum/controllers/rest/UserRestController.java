package com.example.team6forum.controllers.rest;

import com.example.team6forum.exceptions.DuplicateEntityException;
import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.exceptions.UnauthorizedException;
import com.example.team6forum.helpers.AuthenticationHelper;
import com.example.team6forum.helpers.UserMapper;
import com.example.team6forum.helpers.UserOutMapper;
import com.example.team6forum.models.User;
import com.example.team6forum.models.UserInDto;
import com.example.team6forum.models.UserOutDto;
import com.example.team6forum.services.contracts.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final UserOutMapper userOutMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserRestController(UserService userService, UserMapper userMapper,
                              UserOutMapper userOutMapper,
                              AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.userOutMapper = userOutMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<UserOutDto> get() {
        List<User> users = userService.getAllUsers();
        return userOutMapper.userListToOutDtoList(users);
    }

    @GetMapping("/filter")
    public List<UserOutDto> filter(
            @RequestParam(required = false) Optional<String> username,
            @RequestParam(required = false) Optional<String> firstName,
            @RequestParam(required = false) Optional<String> lastName,
            @RequestParam(required = false) Optional<LocalDate> creationDate,
            @RequestParam(required = false) Optional<Integer> userType,
            @RequestParam(required = false) Optional<String> sortBy,
            @RequestParam(required = false) Optional<String> sortOrder
    ) {
        try {
            List<User> users = userService.filter(username, firstName, lastName, creationDate, userType, sortBy, sortOrder);
            return userOutMapper.userListToOutDtoList(users);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public UserOutDto get(@PathVariable int id) {
        try {
            User user = userService.getUserById(id);
            return userOutMapper.userToOutDto(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public void post(@Valid @RequestBody UserInDto userInDto) {
        try {
            User user = userMapper.dtoToUser(userInDto);
            userService.createUser(user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody UserInDto userInDto) {
        try {
            User currentUser = authenticationHelper.tryGetUser(headers);
            User userToUpdate = userMapper.dtoToUser(id, userInDto);
            userService.updateUser(currentUser, userToUpdate);
            return userToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User currentUser = authenticationHelper.tryGetUser(headers);
            userService.deleteUser(currentUser, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
