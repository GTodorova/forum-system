package com.example.team6forum.controllers.mvc;

import com.example.team6forum.exceptions.DuplicateEntityException;
import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.exceptions.UnauthorizedException;
import com.example.team6forum.helpers.AuthenticationHelper;
import com.example.team6forum.helpers.CommentMapper;
import com.example.team6forum.helpers.PostMapper;
import com.example.team6forum.helpers.PostOutMapper;
import com.example.team6forum.models.*;
import com.example.team6forum.services.contracts.CommentService;
import com.example.team6forum.services.contracts.PostService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping("/posts")
public class PostMvcController {
    private final PostService postService;
    private final PostMapper postMapper;
    private final PostOutMapper postOutMapper;
    private final AuthenticationHelper authenticationHelper;
    private final CommentService commentService;
    private final CommentMapper commentMapper;

    @Autowired
    public PostMvcController(PostService postService,
                             PostMapper postMapper,
                             CommentService commentService,
                             CommentMapper commentMapper,
                             AuthenticationHelper authenticationHelper, PostOutMapper postOutMapper) {
        this.postService = postService;
        this.postMapper = postMapper;
        this.commentService = commentService;
        this.commentMapper = commentMapper;
        this.authenticationHelper = authenticationHelper;
        this.postOutMapper = postOutMapper;
    }

    @ModelAttribute("tags")
    public List<Tag> allTags() {
        return postService.getTags();
    }

    @GetMapping("/{id}")
    public String showSinglePost(@Valid @PathVariable int id, Model model,  @ModelAttribute("comment") CommentCreateDto commentCreateDto, BindingResult bindingResult, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedException e) {
            return "unauthorized";
        }
        if (bindingResult.hasErrors()) {
            return "redirect:/posts/{id}/comment/new";
        }

        try {
            Post post = postService.get(id);
            model.addAttribute("comments", commentService.getCommentsByPostId(id));
            model.addAttribute("post", postOutMapper.postToOutDto(post));
            return "PostView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping
    public String showAllPosts(@ModelAttribute("filterOptions") PostFilterDto postFilterDto, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
            PostFilterOptions postFilterOptions = new PostFilterOptions(
                    postFilterDto.getTitle(),
                    postFilterDto.getContent(),
                    postFilterDto.getUsername(),
                    postFilterDto.getStartDate(),
                    postFilterDto.getEndDate(),
                    postFilterDto.getTagId(),
                    postFilterDto.getSortBy(),
                    postFilterDto.getSortOrder());
            List<Post> posts = postService.getAllPostsFilter(postFilterOptions);
            model.addAttribute("filterOptions", postFilterDto);
            model.addAttribute("posts", postOutMapper.postToOutDto(posts));
            return "PostsView";
        } catch (UnauthorizedException e) {
            return "unauthorized";
        } catch (UnsupportedOperationException e) {
            return "PostsView";
        }
    }

    @GetMapping("/new")
    public String showNewPostPage(Model model, HttpSession session, HttpServletRequest request) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedException e) {
            return "unauthorized";
        }
        model.addAttribute("httpServletRequest", request);
        model.addAttribute("post", new PostDto());
        return "PostCreateView";
    }

    @PostMapping("/new")
    public String createPost(@Valid @ModelAttribute("post") PostDto postDto,
                             BindingResult bindingResult,
                             HttpServletRequest request,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("httpServletRequest", request.getRequestURI());
        if (bindingResult.hasErrors()) {
            return "PostCreateView";
        }

        try {
            Post post = postMapper.fromDto(postDto);
            postService.create(post, user);
            return "redirect:/";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("title", "duplicate_post", e.getMessage());
            return "PostCreateView";
        } catch (UnauthorizedException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }
    }


    @GetMapping("/{id}/update")
    public String showEditPostPage(@PathVariable int id, Model model, HttpSession session) {
        try {
           User currentUser =  authenticationHelper.tryGetCurrentUser(session);
           Post post = postService.get(id);
            if((currentUser.getUserId() != post.getUser().getUserId() || currentUser.isBlocked())
                    && !currentUser.isAdmin()){
                return "unauthorized";
            }
        } catch (UnauthorizedException e) {
            return "unauthorized";
        }

        try {
            Post post = postService.get(id);
            PostDto postDto = postMapper.toDto(post);
            model.addAttribute("postId", id);
            model.addAttribute("post", postDto);
            return "PostUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{id}/update")
    public String updatePost(@PathVariable int id,
                             @Valid @ModelAttribute("post") PostDto dto,
                             BindingResult bindingResult, Model model,
                             HttpSession session, HttpServletRequest request) {
        model.addAttribute("httpServletRequest", request.getRequestURI());
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "PostUpdateView";
        }
        try {
            Post post = postMapper.fromDto(id, dto);
            postService.update(post, user);
            return "redirect:/posts/{id}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("title", "duplicate_post", e.getMessage());
            return "PostUpdateView";
        } catch (UnauthorizedException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }
    }

    @GetMapping("/{id}/delete")
    public String deletePost(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedException e) {
            return "redirect:/auth/login";
        }

        try {
            postService.delete(id, user);
            return "redirect:/posts";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }
    }

    @GetMapping("/{id}/comment/new")
    public String createComment(HttpServletRequest request, @PathVariable
    int id, Model model, HttpSession session) {


        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("requestURI", request.getRequestURI());
        model.addAttribute("post", postService.get(id));
        model.addAttribute("comment", new CommentCreateDto());
        return "comment-new";
    }

    @PostMapping("/{id}/comment/new")
    public String createComment(@PathVariable
                                int id, @Valid @ModelAttribute("comment") CommentCreateDto commentCreateDto,
                                BindingResult bindingResult,
                                Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedException e) {
            return "redirect:/auth/login";
        }
        if (bindingResult.hasErrors()) {
            return "redirect:/posts/{id}";
        }

        Comment comment = commentMapper.dtoCreateToObject(commentCreateDto, user, id);
        model.addAttribute("post", postService.get(id));
        commentService.createComment(comment, user);
        return "redirect:/posts/{id}";
    }

    @GetMapping("/{postId}/comment/{id}")
    public String showSingleComment(@PathVariable int postId, @PathVariable int id,
                                    Model model, HttpServletRequest request, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedException e) {
            return "redirect:/auth/login";
        }
        try {
            Comment comment = commentService.getById(id);
            Post post = postService.get(postId);
            model.addAttribute("requestURI", request.getRequestURI());
            model.addAttribute("post", post);
            model.addAttribute("comment", comment);
            return "single-comment";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{postId}/comment/{commentId}/update")
    public String showEditCommentPage(HttpServletRequest request, @PathVariable int postId,
                                      @PathVariable int commentId, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedException e) {
            return "redirect:/auth/login";
        }
        try {
            Post post = postService.get(postId);
            Comment comment = commentService.getById(commentId);
            CommentCreateDto commentCreateDto = commentMapper.toDto(comment);
            model.addAttribute("requestURI", request.getRequestURI());
            model.addAttribute("postId", post.getPostId());
            model.addAttribute("commentId", comment.getCommentId());
            model.addAttribute("comment", commentCreateDto);
            return "comment-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{postId}/comment/{commentId}/update")
    public String commentUpdate(@Valid @PathVariable int postId,
                                @PathVariable int commentId,
                                @ModelAttribute("comment") CommentCreateDto dto,
                                BindingResult bindingResult,
                                Model model,
                                HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedException e) {
            return "redirect:/auth/login";
        }
        if (bindingResult.hasErrors()) {
            return "comment-update";
        }
        try {
            Comment comment = commentMapper.dtoUpdateToObject(commentService.getById(commentId),
                    dto, user.getUserId());
            commentService.update(comment, user);
            return "redirect:/posts/{postId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }

    }

    @GetMapping("/{postId}/comment/{id}/delete")
    public String deleteComment(@PathVariable int postId, @PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedException e) {
            return "redirect:/auth/login";
        }

        try {
            commentService.delete(id, user);
            return "redirect:/posts/{postId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }
    }

    @GetMapping("/comments")
    public String showAllComments(@ModelAttribute("filterOptions") CommentFilterDto commentFilterDto, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
            CommentFilterOptions filterOptions = new CommentFilterOptions(
                    commentFilterDto.getUsername(),
                    commentFilterDto.getPostName(),
                    commentFilterDto.getContent(),
                    commentFilterDto.getStartDate(),
                    commentFilterDto.getEndDate(),
                    commentFilterDto.getSortBy(),
                    commentFilterDto.getSortOrder());
            List<Comment> comments = commentService.getAll(filterOptions);
            model.addAttribute("filterOptions", commentFilterDto);
            model.addAttribute("comments", comments);
            return "CommentsView";
        } catch (UnauthorizedException e) {
            return "unauthorized";
        }
    }

    @GetMapping("/{id}/likes/new")
    public String createLike(HttpServletRequest request, @PathVariable
    int id, Model model, HttpSession session) {
        User curentUser;
        try {
            curentUser = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedException e) {
            return "redirect:/auth/login";
        }
        Post post = postService.get(id);
        postService.createLike(curentUser, post);

        model.addAttribute("requestURI", request.getRequestURI());
        model.addAttribute("post", post);
        model.addAttribute("comment", new CommentCreateDto());
        return "redirect:/posts/{id}";
    }

    @GetMapping("/{id}/likes/delete")
    public String deleteLike(HttpServletRequest request, @PathVariable
    int id, Model model, HttpSession session) {
        User curentUser;
        try {
            curentUser = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedException e) {
            return "redirect:/auth/login";
        }
        Post post = postService.get(id);
        postService.deleteLike(post, curentUser);

        model.addAttribute("requestURI", request.getRequestURI());
        model.addAttribute("post", post);
        model.addAttribute("comment", new CommentCreateDto());
        return "redirect:/posts/{id}";
    }
}
