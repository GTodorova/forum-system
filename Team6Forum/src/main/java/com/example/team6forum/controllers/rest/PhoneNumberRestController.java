package com.example.team6forum.controllers.rest;

import com.example.team6forum.exceptions.DuplicateEntityException;
import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.exceptions.UnauthorizedException;
import com.example.team6forum.helpers.AuthenticationHelper;
import com.example.team6forum.helpers.PhoneNumberMapper;
import com.example.team6forum.helpers.PhoneNumberOutMapper;
import com.example.team6forum.models.PhoneNumber;
import com.example.team6forum.models.PhoneNumberDto;
import com.example.team6forum.models.PhoneNumberOutDto;
import com.example.team6forum.models.User;
import com.example.team6forum.services.PhoneNumberServiceImpl;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/phone-numbers")

public class PhoneNumberRestController {

    public static final String DELETED_NUMBER_MESSAGE = "The phone number was successfully deleted.";
    private final PhoneNumberServiceImpl service;

    private final AuthenticationHelper authenticationHelper;

    private final PhoneNumberOutMapper phoneNumberOutMapper;

    private final PhoneNumberMapper phoneNumberMapper;

    @Autowired
    public PhoneNumberRestController(PhoneNumberServiceImpl service, AuthenticationHelper authenticationHelper, PhoneNumberOutMapper phoneNumberOutMapper, PhoneNumberMapper phoneNumberMapper) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.phoneNumberOutMapper = phoneNumberOutMapper;
        this.phoneNumberMapper = phoneNumberMapper;
    }

    @GetMapping
    public List<PhoneNumberOutDto> get() {

        List<PhoneNumber> numbers = service.getAll();
        List<PhoneNumberOutDto> numbersDto = new ArrayList<>();
        for (PhoneNumber number : numbers) {
            numbersDto.add(phoneNumberOutMapper.phoneNumberOutDto(number, number.getUser()));
        }
        return numbersDto;
    }

    @GetMapping("/{id}")
    public PhoneNumberOutDto get(@PathVariable int id) {

        try {
            return phoneNumberOutMapper.phoneNumberOutDto(service.getById(id), service.getById(id).getUser());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @PostMapping
    public PhoneNumberOutDto create(@RequestHeader HttpHeaders headers, @Valid @RequestBody PhoneNumberDto phoneNumberDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            PhoneNumber phoneNumber = phoneNumberMapper.dtoToObject(new PhoneNumber(), phoneNumberDto);
            service.create(phoneNumber, user);
            return phoneNumberOutMapper.phoneNumberOutDto(phoneNumber, user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public PhoneNumberOutDto update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody PhoneNumberDto phoneNumberDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            PhoneNumber phoneNumber = phoneNumberMapper.dtoToObject(id, phoneNumberDto);
            service.update(phoneNumber, user);
            return phoneNumberOutMapper.phoneNumberOutDto(phoneNumber, user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public String delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            service.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        return DELETED_NUMBER_MESSAGE;
    }
}
