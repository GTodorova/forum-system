package com.example.team6forum.models;

public class UserOutDto {
    private final String userName;
    private final String firstName;
    private final String lastName;
    private final String email;
    private final String userType;

    public UserOutDto(String userName, String firstName, String lastName, String email, String userType) {
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.userType = userType;
    }

    public String getUserName() {
        return userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getUserType() {
        return userType;
    }
}
