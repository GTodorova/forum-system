package com.example.team6forum.models;

import java.util.List;

public class PostTagDto {

    private List<String> tagNames;

    public PostTagDto() {
    }

    public List<String> getTagNames() {
        return tagNames;
    }

    public void setTagNames(List<String> tagNames) {
        this.tagNames = tagNames;
    }

}
