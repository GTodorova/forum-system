package com.example.team6forum.models;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class CommentUpdateDto {

    @NotNull(message = "Content can't be null")
    @Size(max = 255, message = "Comment should be at most 255 symbols")
    private String content;

    public CommentUpdateDto() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
