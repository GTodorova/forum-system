package com.example.team6forum.models;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class UserInDto {
    @NotNull(message = "Username can't be null")
    @Size(min = 4, max = 32, message = "First name should be between 4 and 32 symbols")
    private String userName;
    @NotNull(message = "Password can't be null")
    @Size(min = 4, max = 32, message = "Password should be between 4 and 32 symbols")
    private String password;
    @NotNull(message = "First name can't be null")
    @Size(min = 4, max = 32, message = "First name should be between 4 and 32 symbols")
    private String firstName;
    @NotNull(message = "Last name can't be null")
    @Size(min = 4, max = 32, message = "Last name should be between 4 and 32 symbols")
    private String lastName;
    @NotNull(message = "Email name can't be null")
    @Size(min = 4, max = 32, message = "Email should be between 4 and 32 symbols")
    private String email;
    private int userTypeId;
    private String phoneNumber;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(int userTypeId) {
        this.userTypeId = userTypeId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
