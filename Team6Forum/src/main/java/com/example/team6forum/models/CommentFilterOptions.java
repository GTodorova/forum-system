package com.example.team6forum.models;

import java.time.LocalDate;
import java.util.Optional;

public class CommentFilterOptions {

    private Optional<String> username;
    private Optional<String> postName;
    private Optional<String> content;

    private Optional<LocalDate> startDate;

    private Optional<LocalDate> endDate;

    private Optional<String> sortBy;

    private Optional<String> sortOrder;

    public CommentFilterOptions() {
        this(null, null, null, null, null, null, null);
    }

    public CommentFilterOptions(String username,
                                String postName,
                                String content, LocalDate startDate, LocalDate endDate, String sortBy,
                                String sortOrder) {
        this.username = Optional.ofNullable(username);
        this.postName = Optional.ofNullable(postName);
        this.content = Optional.ofNullable(content);
        this.startDate = Optional.ofNullable(startDate);
        this.endDate = Optional.ofNullable(endDate);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);

    }

    public Optional<String> getUsername() {
        return username;
    }


    public Optional<String> getPostName() {
        return postName;
    }

    public Optional<LocalDate> getStartDate() {
        return startDate;
    }

    public Optional<LocalDate> getEndDate() {
        return endDate;
    }

    public Optional<String> getContent() {
        return content;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }
}
