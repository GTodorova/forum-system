package com.example.team6forum.models;

import java.time.LocalDate;
import java.util.Optional;

public class PostFilterOptions {

    private Optional<String> title;
    private Optional<String> content;
    private Optional<String> username;
    private Optional<LocalDate> startDate;

    private Optional<LocalDate> endDate;

    private Optional<Integer> tagId;
    private Optional<String> sortBy;

    private Optional<String> sortOrder;

    public PostFilterOptions() {
        this(null, null, null, null, null, null, null, null);
    }

    public PostFilterOptions(String title,
                             String content, String username,
                             LocalDate startDate, LocalDate endDate, Integer tagId, String sortBy,
                             String sortOrder) {
        this.title = Optional.ofNullable(title);
        this.content = Optional.ofNullable(content);
        this.username = Optional.ofNullable(username);
        this.startDate = Optional.ofNullable(startDate);
        this.endDate = Optional.ofNullable(endDate);
        this.tagId = Optional.ofNullable(tagId);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);

    }

    public Optional<String> getTitle() {
        return title;
    }

    public Optional<String> getContent() {
        return content;
    }

    public Optional<String> getUsername() {
        return username;
    }

    public Optional<LocalDate> getStartDate() {
        return startDate;
    }

    public Optional<LocalDate> getEndDate() {
        return endDate;
    }

    public Optional<Integer> getTagId() {
        return tagId;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }
}
