package com.example.team6forum.models;

import java.io.Serializable;
import java.util.Objects;

public class PostLikePK implements Serializable {

    private int postId;
    private int userId;

    public PostLikePK() {
    }

    public PostLikePK(int postId, int userId) {
        this.postId = postId;
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostLikePK that = (PostLikePK) o;
        return postId == that.postId && userId == that.userId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(postId, userId);
    }
}
