package com.example.team6forum.models;

import java.time.LocalDate;
import java.util.Set;

public class PostOutDto {

    private int postId;
    private String title;
    private String content;
    private LocalDate creationDate;
    private UserOutDto user;

    private Set<Tag> tags;

    private Set<UserOutDto> postLikers;

    public PostOutDto() {
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public UserOutDto getUser() {
        return user;
    }

    public void setUser(UserOutDto user) {
        this.user = user;
    }

    public Set<UserOutDto> getPostLikers() {
        return postLikers;
    }

    public void setPostLikers(Set<UserOutDto> postLikeRelationSet) {
        this.postLikers = postLikeRelationSet;
    }

    public boolean isLiked(String username) {
        return getPostLikers().stream().anyMatch(l -> l.getUserName().equals(username));
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }
}
