package com.example.team6forum.models;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class PhoneNumberDto {

    @NotNull(message = "Phone number can't be null")
    @Size(max = 255, message = "Phone number should be at most 20 symbols")
    private String phoneNumber;

    @NotNull(message = "User id can't be null")
    private int userId;

    public PhoneNumberDto() {
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
