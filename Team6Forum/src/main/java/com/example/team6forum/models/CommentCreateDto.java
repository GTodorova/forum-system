package com.example.team6forum.models;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import org.springframework.stereotype.Component;


@Component
public class CommentCreateDto {

    @NotEmpty(message = "Content can't be null")
    @Size(max = 255, message = "Comment should be at most 255 symbols")
    private String content;

    public CommentCreateDto() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
