package com.example.team6forum.models;

import java.time.LocalDate;

public class CommentOutDTO {


    private int commentId;
    private String content;
    private String username;
    private int userId;
    private LocalDate creationDate;

    public CommentOutDTO() {
    }

    public CommentOutDTO(String content, String username, LocalDate creationDate) {
        this.content = content;
        this.username = username;
        this.creationDate = creationDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
