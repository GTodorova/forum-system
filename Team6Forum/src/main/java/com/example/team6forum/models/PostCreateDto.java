package com.example.team6forum.models;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class PostCreateDto {
    @NotNull(message = "Title must not be null")
    @Size(min = 16, max = 64, message = "Title must be between 16 and 64 symbols")
    private String title;

    @NotNull(message = "Content must not be null")
    @Size(min = 32, max = 8192, message = "Content must be between 32 and 8192 symbols")
    private String content;

    public PostCreateDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
