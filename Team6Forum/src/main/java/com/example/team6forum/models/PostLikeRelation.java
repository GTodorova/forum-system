package com.example.team6forum.models;

import jakarta.persistence.*;

@Entity
@Table(name = "post_like")
@IdClass(PostLikePK.class)
public class PostLikeRelation {
    @Id
    @Column(name = "post_id")
    int postId;
    @Id
    @Column(name = "user_id")
    int userId;

    public PostLikeRelation() {

    }

    public PostLikeRelation(int userId, int postId) {
        this.userId = userId;
        this.postId = postId;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
