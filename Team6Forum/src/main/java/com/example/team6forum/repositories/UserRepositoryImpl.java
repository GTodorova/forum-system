package com.example.team6forum.repositories;

import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.models.User;
import com.example.team6forum.models.UserFilterOptions;
import com.example.team6forum.models.UserRole;
import com.example.team6forum.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.*;

@Repository
public class UserRepositoryImpl implements UserRepository {
    public static final String INVALID_ORDERBY_PARAMETER = "Invalid orderBy parameter";
    public static final String INVALID_SORT_ORDER_PARAMETER = "Invalid sortOrder parameter";
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAllUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            return query.list();
        }
    }

    @Override
    public User getUserById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public User getUserByUsername(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where userName = :name", User.class);
            query.setParameter("name", name);
            List<User> result = query.list();
            if (result.isEmpty()) {
                throw new EntityNotFoundException("User", "userName", name);
            }
            return result.get(0);
        }
    }

    public User getUserByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);
            List<User> result = query.list();
            if (result.isEmpty()) {
                throw new EntityNotFoundException("User", "email", email);
            }
            return result.get(0);
        }
    }

    @Override
    public void createUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void updateUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteUser(int id) {
        User userToDelete = getUserById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(userToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<User> filter(Optional<String> username, Optional<String> firstName, Optional<String> lastName,
                             Optional<LocalDate> creationDate, Optional<Integer> userType,
                             Optional<String> sortBy, Optional<String> sortOrder) {

        StringBuilder queryString = new StringBuilder(" from User ");
        Map<String, Object> queryParams = new HashMap<>();
        ArrayList<String> filter = new ArrayList<>();

        username.ifPresent(v -> {
            filter.add(" userName like :username ");
            queryParams.put("username", "%" + v + "%");
        });

        firstName.ifPresent(v -> {
            filter.add(" firstName like :firstName ");
            queryParams.put("firstName", "%" + v + "%");
        });

        lastName.ifPresent(v -> {
            filter.add(" lastName like :lastName ");
            queryParams.put("lastName", "%" + v + "%");
        });

        userType.ifPresent(v -> {
            filter.add(" userRole.userRoleId = :userRole ");
            queryParams.put("userRole", v);
        });

        creationDate.ifPresent(v -> {
            filter.add(" creationDate = :creationDate ");
            queryParams.put("creationDate", v);
        });

        if (!filter.isEmpty()) {
            queryString.append(" where ").append(String.join(" and ", filter));
        }

        sortBy.ifPresentOrElse(value -> queryString.append(generateStringFromSortBy(value)),
                () -> queryString.append(generateStringFromSortBy("date")));

        sortOrder.ifPresentOrElse(value -> {
                    if (value.equals("desc")) {
                        queryString.append(" desc ");
                    } else if (value.equals("asc") || value.equals("")) {
                        queryString.append(" asc ");
                    } else {
                        throw new UnsupportedOperationException(INVALID_SORT_ORDER_PARAMETER);
                    }
                },
                () -> queryString.append(" asc "));

        try (Session session = sessionFactory.openSession()) {
            Query<User> queryList = session.createQuery(queryString.toString(), User.class);
            queryList.setProperties(queryParams);

            return queryList.list();
        }
    }

    @Override
    public List<UserRole> getAllRoles() {
        try (Session session = sessionFactory.openSession()) {
            Query<UserRole> query = session.createQuery("from UserRole ", UserRole.class);
            return query.list();
        }
    }

    @Override
    public List<User> getAllUsers(UserFilterOptions userFilterOptions) {
        return filter(userFilterOptions.getUsername(), userFilterOptions.getFirstName(), userFilterOptions.getLastName(),
                userFilterOptions.getCreationDate(), userFilterOptions.getUserTypeId(), userFilterOptions.getSortBy(),
                userFilterOptions.getSortOrder());
    }

    private String generateStringFromSortBy(String value) {
        StringBuilder queryString = new StringBuilder(" order by ");

        switch (value) {
            case "username" -> queryString.append(" userName ");
            case "date" -> queryString.append(" creationDate ");
            case "userRole" -> queryString.append(" userRole.userRoleId ");
            default -> throw new UnsupportedOperationException(
                    INVALID_ORDERBY_PARAMETER);
        }

        return queryString.toString();
    }
}
