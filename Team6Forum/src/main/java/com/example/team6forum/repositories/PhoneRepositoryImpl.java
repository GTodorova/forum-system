package com.example.team6forum.repositories;

import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.models.PhoneNumber;
import com.example.team6forum.repositories.contracts.PhoneRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PhoneRepositoryImpl implements PhoneRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PhoneRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<PhoneNumber> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<PhoneNumber> query = session.createQuery("from PhoneNumber", PhoneNumber.class);
            return query.list();
        }
    }

    @Override
    public PhoneNumber getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            PhoneNumber phoneNumber = session.get(PhoneNumber.class, id);
            if (phoneNumber == null) {
                throw new EntityNotFoundException("Phone number", id);
            }
            return phoneNumber;
        }
    }

    @Override
    public PhoneNumber getByNumber(String number) {
        try (Session session = sessionFactory.openSession()) {
            PhoneNumber phoneNumber = session.get(PhoneNumber.class, number);
            if (phoneNumber == null) {
                throw new EntityNotFoundException("Phone number", "number", number);
            }
            return phoneNumber;
        }
    }

    @Override
    public void create(PhoneNumber number) {
        try (Session session = sessionFactory.openSession()) {
            session.persist(number);
        }
    }

    @Override
    public void update(PhoneNumber number) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(number);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        PhoneNumber numberToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(numberToDelete);
            session.getTransaction().commit();
        }
    }
}
