package com.example.team6forum.repositories.contracts;

import com.example.team6forum.models.User;
import com.example.team6forum.models.UserFilterOptions;
import com.example.team6forum.models.UserRole;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface UserRepository {
    List<User> getAllUsers();

    User getUserById(int id);

    User getUserByUsername(String name);

    User getUserByEmail(String email);

    void createUser(User user);

    void updateUser(User user);

    void deleteUser(int id);


    List<User> filter(Optional<String> username, Optional<String> firstName, Optional<String> lastName,
                      Optional<LocalDate> creationDate, Optional<Integer> userType,
                      Optional<String> sortBy, Optional<String> sortOrder);

    List<UserRole> getAllRoles();

    List<User> getAllUsers(UserFilterOptions userFilterOptions);
}
