package com.example.team6forum.repositories;

import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.models.Post;
import com.example.team6forum.models.PostFilterOptions;
import com.example.team6forum.models.PostLikeRelation;
import com.example.team6forum.models.Tag;
import com.example.team6forum.repositories.contracts.PostRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.*;

@Repository
@PropertySource("classpath:application.properties")
public class PostRepositoryImpl implements PostRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PostRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Post get(int id) {
        try (Session session = sessionFactory.openSession()) {
            Post post = session.get(Post.class, id);
            if (post == null) {
                throw new EntityNotFoundException("Post", id);
            }
            return post;
        }
    }

    public List<Tag> getTags() {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag", Tag.class);
            return query.list();
        }
    }

    @Override
    public List<Post> getAllPosts() {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery("from Post", Post.class);
            return query.list();
        }
    }

    @Override
    public Post get(String title) {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery("from Post where title = :title", Post.class);
            query.setParameter("title", title);

            List<Post> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Post", "title", title);
            }

            return result.get(0);
        }
    }

    @Override
    public void create(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(post);
            session.getTransaction().commit();
        }
    }

    @Override
    public void createTag(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(tag);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Post post) {
        try (Session session = sessionFactory.openSession()) {
            Post postToUpdate = get(post.getPostId());
            postToUpdate.setTitle(post.getTitle());
            postToUpdate.setContent(post.getContent());
            postToUpdate.setTags(post.getTags());
            postToUpdate.setPostLikers(post.getPostLikers());
            session.beginTransaction();
            session.merge(postToUpdate);
            session.getTransaction().commit();
        }
    }

    public void updateTag(Post post, Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            if (!getTagNames().contains(tag.getTitle())) {
                session.merge(post);
                session.getTransaction().commit();
            }
        }
    }

    @Override
    public List<Post> searchByTagName(Optional<String> tagName) {
        if (tagName.isEmpty()) {
            return getAll();
        }
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery(
                    " select distinct p from Post p join p.tags t where t.title like :tagName ", Post.class
            );
            query.setParameter("tagName", "%" + tagName.get() + "%");
            return query.list();
        }
    }

    @Override
    public void delete(int id) {
        Post postToDelete = get(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(postToDelete);
            session.getTransaction().commit();
        }
    }


    @Override
    public List<Post> filter(Optional<String> username, Optional<String> content, Optional<LocalDate> startDate,
                             Optional<LocalDate> endDate, Optional<String> title,
                             Optional<Integer> tagId,
                             Optional<String> sortBy, Optional<String> sortOrder) {

        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" Select p from Post p JOIN p.user u LEFT JOIN " +
                    " p.tags t ");
            ArrayList<String> filter = new ArrayList<>();
            Map<String, Object> queryParams = new HashMap<>();

            title.ifPresent(value -> {
                filter.add(" p.title like :title ");
                queryParams.put("title", "%" + value + "%");
            });

            content.ifPresent(value -> {
                filter.add(" p.content like :content ");
                queryParams.put("content", "%" + value + "%");
            });

            username.ifPresent(value -> {
                filter.add(" u.userName like :username ");
                queryParams.put("username", "%" + value + "%");
            });

            startDate.ifPresent(value -> {
                filter.add(" p.creationDate >= :startDate ");
                queryParams.put("startDate", value);
            });

            endDate.ifPresent(value -> {
                filter.add(" p.creationDate <= :endDate ");
                queryParams.put("endDate", value);
            });

            tagId.ifPresent(value -> {
                filter.add(" t.tagId = :tagId ");
                queryParams.put("tagId", value);
            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            sortBy.ifPresent(value -> queryString.append(generateStringFromSort(value)));


            Query<Post> queryList = session.createQuery(queryString.toString(), Post.class);
            queryList.setProperties(queryParams);

            return queryList.list();
        }
    }

    @Override
    public List<Post> getAll(Optional<String> search) {
        if (search.isEmpty()) {
            return getAll();
        }

        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery(" from Post where " +
                    "title like :title or content like :content");
            query.setParameter("title", "%" + search.get() + "%");
            query.setParameter("content", "%" + search.get() + "%");
            return query.list();
        }
    }

    @Override
    public List<Post> top10newest() {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery(
                    "from Post ORDER by creationDate desc ", Post.class
            );
            return query.setMaxResults(10).list();
        }
    }

    @Override
    public List<Post> mostCommented() {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery(
                    "select p from Comment c join c.post p group by p ", Post.class
            );

            return query.setMaxResults(10).list();
        }
    }

    @Override
    public long getNumberOfPosts() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("select count( distinct p.postId ) from Post p ", Long.class).getSingleResult();
        }
    }

    @Override
    public List<PostLikeRelation> getAllLikes() {
        try (Session session = sessionFactory.openSession()) {
            Query<PostLikeRelation> query = session.createQuery("from PostLikeRelation ", PostLikeRelation.class);
            return query.list();
        }
    }

    @Override
    public boolean checkIfPostLikeRealtionExists(int postId, int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<PostLikeRelation> query = session
                    .createQuery(" from PostLikeRelation " +
                            "where postId = :postId and userId = :userId ", PostLikeRelation.class);
            query.setParameter("userId", userId);
            query.setParameter("postId", postId);

            return !query.getResultList().isEmpty();
        }
    }

    @Override
    public void creteLike(PostLikeRelation postLikeRelation) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.persist(postLikeRelation);
            session.getTransaction().commit();
        }
    }

    public List<String> getTagNames() {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag", Tag.class);
            List<String> tagTitles = new ArrayList<>();
            List<Tag> tags = query.list();
            for (Tag tag : tags) {
                tagTitles.add(tag.getTitle());
            }
            return tagTitles;
        }
    }

    public Tag getTagByName(String title) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where title = :title", Tag.class);
            query.setParameter("title", title);
            List<Tag> result = query.list();
            if (result.isEmpty()) {
                throw new EntityNotFoundException("Tag", "title", title);
            }
            return result.get(0);
        }
    }

    public Tag getTagById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = session.get(Tag.class, id);
            if (tag == null) {
                throw new EntityNotFoundException("Tag", id);
            }
            return tag;
        }
    }

    @Override
    public List<Post> getAllPostsFilter(PostFilterOptions postFilterOptions) {
        return filter(postFilterOptions.getUsername(), postFilterOptions.getContent(),
                postFilterOptions.getStartDate(), postFilterOptions.getEndDate(), postFilterOptions.getTitle(),
                postFilterOptions.getTagId(), postFilterOptions.getSortBy(), postFilterOptions.getSortOrder());
    }


    private List<Post> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery("from Post", Post.class);
            return query.list();
        }
    }

    private String generateStringFromSort(String value) {
        StringBuilder queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");

        if (value.isEmpty()) {
            return "";
        }

        switch (params[0]) {
            case "title":
                queryString.append(" p.title ");
                break;
            case "content":
                queryString.append(" p.content ");
                break;
            case "username":
                queryString.append(" u.userName ");
                break;
            case "startDate":
            case "endDate":
                queryString.append(" p.creationDate");
                break;

            default:
                throw new UnsupportedOperationException(
                        "Sort should have max two params divided by _ symbol!");
        }

        if (params.length > 2) {
            throw new UnsupportedOperationException(
                    "Sort should have max two params divided by _ symbol!");
        }

        if (params.length == 2 && params[1].equalsIgnoreCase("desc")) {
            queryString.append(" desc ");
        }
        return queryString.toString();
    }
}