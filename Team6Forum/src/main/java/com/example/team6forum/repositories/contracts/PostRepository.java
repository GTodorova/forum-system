package com.example.team6forum.repositories.contracts;

import com.example.team6forum.models.Post;
import com.example.team6forum.models.PostFilterOptions;
import com.example.team6forum.models.PostLikeRelation;
import com.example.team6forum.models.Tag;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface PostRepository {

    Post get(int id);

    List<Post> getAllPosts();

    List<Tag> getTags();

    Post get(String title);

    Tag getTagByName(String title);

    List<String> getTagNames();

    void updateTag(Post post, Tag tag);

    List<Post> searchByTagName(Optional<String> tagName);

    void create(Post post);

    void update(Post post);

    void delete(int id);

    List<Post> filter(Optional<String> username, Optional<String> content, Optional<LocalDate> startDate,
                      Optional<LocalDate> endDate, Optional<String> title,
                      Optional<Integer> tagId,
                      Optional<String> sortBy, Optional<String> sortOrder);

    List<Post> getAll(Optional<String> search);

    List<Post> top10newest();

    List<Post> mostCommented();

    void createTag(Tag tag);

    long getNumberOfPosts();

    List<PostLikeRelation> getAllLikes();

    boolean checkIfPostLikeRealtionExists(int postId, int userId);

    void creteLike(PostLikeRelation postLikeRelation);

    Tag getTagById(int id);

    List<Post> getAllPostsFilter(PostFilterOptions postFilterOptions);

}
