package com.example.team6forum.repositories.contracts;

import com.example.team6forum.models.PhoneNumber;
import com.example.team6forum.models.PostLikeRelation;

import java.util.List;

public interface PostLikeRepository {
    List<PostLikeRelation> getAllLikes();
}
