package com.example.team6forum.repositories;

import com.example.team6forum.exceptions.EntityNotFoundException;
import com.example.team6forum.models.Comment;
import com.example.team6forum.models.CommentFilterOptions;
import com.example.team6forum.repositories.contracts.CommentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.*;


@Repository
public class CommentRepositoryImpl implements CommentRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CommentRepositoryImpl(SessionFactory sessionFactory) {

        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Comment> getComments(CommentFilterOptions commentFilterOptions) {
        return filter(commentFilterOptions.getUsername(), commentFilterOptions.getContent(),
                commentFilterOptions.getStartDate(), commentFilterOptions.getEndDate(),
                commentFilterOptions.getPostName(), commentFilterOptions.getSortBy(), commentFilterOptions.getSortOrder());
    }

    @Override
    public int getNumberOfComments(int postId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Comment> query = session.createQuery("from Comment where post.postId = :postId", Comment.class);
            query.setParameter("id", postId);
            List<Comment> result = query.list();
            return result.size();
        }
    }

    @Override
    public List<Comment> getAllComments() {
        try (Session session = sessionFactory.openSession()) {
            Query<Comment> query = session.createQuery("from Comment ", Comment.class);
            return query.list();
        }
    }


    @Override
    public List<Comment> getCommentsByUserId(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Comment> query = session.createQuery("from Comment where createdBy.userId = :id_par", Comment.class);
            query.setParameter("id_par", userId);
            return query.list();
        }
    }

    @Override
    public List<Comment> getCommentsByPostId(int postId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Comment> query = session.createQuery("from Comment where post.postId = :id_par", Comment.class);
            query.setParameter("id_par", postId);
            return query.list();
        }
    }

    @Override
    public Comment getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Comment comment = session.get(Comment.class, id);
            if (comment == null) {
                throw new EntityNotFoundException("Comment", id);
            }
            return comment;
        }
    }

    @Override
    public void createComment(Comment comment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.persist(comment);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Comment comment) {
        try (Session session = sessionFactory.openSession()) {
            Comment commentToUpdate = getById(comment.getCommentId());
            commentToUpdate.setContent(comment.getContent());
            session.beginTransaction();
            session.merge(commentToUpdate);
            session.getTransaction().commit();

        }
    }

    @Override
    public void delete(int id) {
        Comment commentToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(commentToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Comment> filter(Optional<String> username, Optional<String> content, Optional<LocalDate> startDate,
                                Optional<LocalDate> endDate, Optional<String> postName,
                                Optional<String> sortBy, Optional<String> sortOrder) {
        try (
                Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" Select c from Comment c JOIN c.createdBy u JOIN c.post p ");
            Map<String, Object> queryParams = new HashMap<>();
            ArrayList<String> filter = new ArrayList<>();
            username.ifPresent(value -> {
                filter.add(" u.userName like :username ");
                queryParams.put("username", "%" + value + "%");
            });

            content.ifPresent(value -> {
                filter.add(" c.content like :content ");
                queryParams.put("content", "%" + value + "%");
            });

            startDate.ifPresent(value -> {
                filter.add(" c.creationDate >= :startDate ");
                queryParams.put("startDate", value);
            });

            endDate.ifPresent(value -> {
                filter.add(" c.creationDate <= :endDate ");
                queryParams.put("endDate", value);
            });


            postName.ifPresent(value -> {
                filter.add(" p.title like :postName ");
                queryParams.put("postName", "%" + value + "%");

            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            sortBy.ifPresent(value -> queryString.append(generateStringFromSort(value)));

            Query<Comment> queryList = session.createQuery(queryString.toString(), Comment.class);
            queryList.setProperties(queryParams);

            return queryList.list();
        }
    }

    private String generateStringFromSort(String value) {
        StringBuilder queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");

        if (value.isEmpty()) {
            return "";
        }

        switch (params[0]) {
            case "content":
                queryString.append(" c.content ");
                break;
            case "username":
                queryString.append(" u.userName ");
                break;
            case "postName":
                queryString.append(" p.title ");
                break;
            case "startDate":
            case "endDate":
                queryString.append(" c.creationDate");
                break;

            default:
                throw new UnsupportedOperationException(
                        "Sort should have max two params divided by _ symbol!");
        }

        if (params.length > 2) {
            throw new UnsupportedOperationException(
                    "Sort should have max two params divided by _ symbol!");
        }

        if (params.length == 2 && params[1].equalsIgnoreCase("desc")) {
            queryString.append(" desc ");
        }
        return queryString.toString();
    }
}

