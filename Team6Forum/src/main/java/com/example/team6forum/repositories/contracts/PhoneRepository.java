package com.example.team6forum.repositories.contracts;

import com.example.team6forum.models.PhoneNumber;

import java.util.List;

public interface PhoneRepository {
    List<PhoneNumber> getAll();

    PhoneNumber getById(int id);

    PhoneNumber getByNumber(String number);

    void create(PhoneNumber number);

    void update(PhoneNumber number);

    void delete(int id);
}
