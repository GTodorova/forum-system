package com.example.team6forum.repositories.contracts;

import com.example.team6forum.models.Comment;
import com.example.team6forum.models.CommentFilterOptions;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface CommentRepository {

    List<Comment> getComments(CommentFilterOptions commentFilterOptions);

    int getNumberOfComments(int postId);

    List<Comment> getAllComments();

    List<Comment> getCommentsByUserId(int userId);

    List<Comment> getCommentsByPostId(int postId);

    Comment getById(int id);

    void createComment(Comment comment);

    void update(Comment comment);

    void delete(int id);

    List<Comment> filter(Optional<String> username, Optional<String> content, Optional<LocalDate> startDate,
                         Optional<LocalDate> endDate, Optional<String> postName,
                         Optional<String> sortBy, Optional<String> sortOrder);
}
