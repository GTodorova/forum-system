package com.example.team6forum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Team6ForumApplication {

    public static void main(String[] args) {
        SpringApplication.run(Team6ForumApplication.class, args);
    }

}
