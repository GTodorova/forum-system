package com.example.team6forum.exceptions;

public class NoEntityFoundException extends RuntimeException {

    public NoEntityFoundException(String type, String attribute, int value) {
        super(String.format("%s in %s with id %d not found", type, attribute, value));
    }
}
